/* Borrowed from http://www2.cs.uidaho.edu/~jeffery/courses/445/symt.h */
/* Modified by Alex McNurlin for University of Idaho CS445 HW 3 */

#ifndef SYMT_H
#define SYMT_H

#include "structs.h"

/* Prototypes */
ParamList* get_parameters(Tree* params);
SymbolTable* new_st(int size);			/* create symbol table */
SymbolTableEntry* get_entry(SymbolTable* st, Tree* s);
SymbolTableEntry* lookup_st(SymbolTable*, char *); /* lookup symbol */
TypeInfo* get_type(Tree* type);
int get_lineno(Tree* n);
int insert_sym(SymbolTable*, Tree*, TypeInfo*);	/* enter symbol into table */
void delete_st(SymbolTable*);			/* destroy symbol table */
void dovariabledeclarator(Tree* n, TypeInfo* t);
void enter_newscope(Tree* n, Tree *s, int typ);
void print_param_list(ParamList* p);
void print_st(SymbolTable* st, int level);
void semantic_analysis(Tree* n);
void populate_symboltables(Tree* n);
SymbolTable* get_scope(char* s);

extern SymbolTable* current;	       /* current */

#define pushscope(stp) do { stp->parent = current; current = stp; } while (0)
#define popscope() do { current = current->parent; } while(0)

#endif					/* SYMTAB_H */
