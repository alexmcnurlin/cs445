#! /usr/bin/env python3

import re
import sys

token_re = re.compile("allocate_tree\((.*?),")
tokens_def = ""
tokens_list = "char* yyprodcodes[] = {0"
with open(sys.argv[1], 'r') as f:
    contents = f.read()
    t = list(set(token_re.findall(contents)))
    t.sort()
    i = 1
    for token in t:
        tokens_def += "#define {} {}\n".format(token.upper(), i)
        tokens_list += ", \n\"{}\"".format(token.lower())
        i += 1
    tokens_def += "#define INT_TO_DOUBLE {}\n".format(i)
    tokens_list += ", \n\"{}\"".format("int_to_double")
tokens_list += "}; \n"

with open("{}.h".format(sys.argv[2]), 'w') as f:
    f.write("#ifndef PRODRULES_H\n#define PRODRULES_H\n")
    f.write(tokens_def)
    f.write("extern char** yyprodcodes;\n")
    f.write("char* pretty_print_name(int prodrule);\n")
    f.write("#endif\n")

with open("{}.c".format(sys.argv[2]), 'w') as f:
    f.write(tokens_list)
    f.write("char* pretty_print_name(int prodrule) {return yyprodcodes[prodrule];}\n")
