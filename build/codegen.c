#include <stdio.h>

#include "helper.h"
#include "parser.tab.h"
#include "prodrules.h"
#include "structs.h"
#include "symt.h"
#include "tac.h"

extern SymbolTable* current;
char** STRINGS_LIST = NULL;
int NSTRINGS = 0;
double* DOUBLES_LIST = NULL;
int NDOUBLES = 0;

void codegen(Tree* n) {
    int i;
    if (n==NULL) return;

    Tree* nc = n;
    Instruction* g;
    SymbolTableEntry* ste;
    // Activate symbol table
    switch (n->prodrule) {
        case CLASSDEF:
            current = get_scope(n->children[1]->leaf->text);
            ste = get_entry(current, n->children[1]);
            break;
        case FUNCTIONDEF:
        case METHODDEF:
            current = get_scope(n->children[1]->leaf->text);
            break;
        case CONSTRUCTOR:
            current = get_scope(n->children[0]->leaf->text);
            break;
    }

    // this is a post-order traversal, so visit children first
    switch(n->prodrule) {

        case DATAMEMBERACCESS:
            /* Ignore method/member names. Those will be checked with the types */
            codegen(n->children[0]);
            break;
        case METHODCALL:
            codegen(n->children[0]);
            codegen(n->children[2]);
            break;
        default:
            for (i = 0; i < n->nChildren; i++) {
                codegen(n->children[i]);
            }
    }

    // back from children, consider what we have to do with
    // this node. The main thing we have to do, one way or
    // another, is assign n->code
    int opc, temp_int, temp_int2;
    /* char* fname; */
    Address* temp, *t1, *t2, *t3, *l1, *l2;
    switch (n->prodrule) {
        case 0:
            switch (n->leaf->category) {
                case IDENT:
                    ste = get_entry(current, n);
                    n->code = emptylist();
                    n->place = ste->place;
                    break;
                case INTLITERAL:
                    n->code = emptylist();
                    n->place = newconst(n->leaf->ival);
                    break;
                case TRUE:
                case FALSE:
                    n->code = emptylist();
                    n->place = newconst(n->leaf->ival);
                    break;
                case STRINGLITERAL:
                    if (NSTRINGS == 0) {
                        STRINGS_LIST = calloc(1, (++NSTRINGS)*sizeof(char*));
                        STRINGS_LIST[0] = n->leaf->sval;
                        n->place = newtemp();
                        n->code = gen(O_ADDR, n->place, newstring(), NULL);
                    } else {
                        STRINGS_LIST = realloc(STRINGS_LIST, (++NSTRINGS)*sizeof(char*));
                        STRINGS_LIST[NSTRINGS-1] = n->leaf->sval;
                        n->place = newtemp();
                        n->code = gen(O_ADDR, n->place, newstring(), NULL);
                    }
                    break;
                case REALLITERAL:
                    if (NDOUBLES == 0) {
                        DOUBLES_LIST = calloc(1, (++NDOUBLES)*sizeof(char*));
                        DOUBLES_LIST[0] = n->leaf->dval;
                        n->place = newtemp();
                        n->code = gen(O_ASN, n->place, newdouble(), NULL);
                    } else {
                        DOUBLES_LIST = realloc(DOUBLES_LIST, (++NDOUBLES)*sizeof(char*));
                        DOUBLES_LIST[NDOUBLES-1] = n->leaf->dval;
                        n->place = newtemp();
                        n->code = gen(O_ASN, n->place, newdouble(), NULL);
                    }
                    break;
            }
            break;

        ///////////////////////////////////////////////////////////////////////
        // Basic Operators
            case ADD:
                opc = O_ADD + op_type(n->children[0]->type);
                n->code = concat(n->children[0]->code, n->children[1]->code);
                n->place = newtemp();
                g = gen(opc, n->place, n->children[0]->place, n->children[1]->place);
                n->code = concat(n->code, g);
                break;

            case SUBTRACT:
                opc = O_SUB + op_type(n->children[0]->type);
                n->code = concat(n->children[0]->code, n->children[1]->code);
                n->place = newtemp();
                g = gen(opc, n->place, n->children[0]->place, n->children[1]->place);
                n->code = concat(n->code, g);
                break;

            case AND_EXP:
                n->code = concat(n->children[0]->code, n->children[1]->code);
                n->place = newtemp();
                g = gen(O_AND, n->place, n->children[0]->place, n->children[1]->place);
                n->code = concat(n->code, g);
                break;

            case OR_EXP:
                n->code = concat(n->children[0]->code, n->children[1]->code);
                n->place = newtemp();
                g = gen(O_OR, n->place, n->children[0]->place, n->children[1]->place);
                n->code = concat(n->code, g);
                break;

            case ARRAYACCESS:
                opc = O_INDEX + op_type(n->children[0]->type);
                if (n->children[1]==NULL) {break;} // TDEFAULT
                n->code = concat(n->children[0]->code, n->children[1]->code);
                n->place = newtemp();
                g = gen(opc, n->place, n->children[0]->place, n->children[1]->place);
                n->code = concat(n->code, g);
                break;

            case ASSIGNMENT:
                // Dereference assignable things
                n->code = concat(n->children[0]->code, n->children[1]->code);
                n->place = n->children[0]->place;
                if (n->children[0]->prodrule == MULTI_ASSIGNMENT) {
                    nc = n->children[0];
                    g = NULL;
                    while (nc->prodrule == MULTI_ASSIGNMENT) {
                        g = concat(n->code, gen(O_ASN, nc->children[1]->place, n->children[1]->place, NULL));
                        nc = nc->children[0];
                    }
                    g = concat(g, gen(O_ASN, nc->place, n->children[1]->place, NULL));
                } else {
                    if (n->children[0]->prodrule == ARRAYACCESS && n->children[0]->children[1] == NULL) {
                        g = gen(O_TDEFAULT, n->children[0]->children[0]->place, n->children[1]->place, NULL);
                    } else {
                        g = gen(O_ASN, n->children[0]->place, n->children[1]->place, NULL);
                    }
                }
                n->code = concat(n->code, g);
                break;

            case INCREMENT:
                if (n->children[0]->type->basetype==LISTTYPE && n->children[1]->type->basetype==LISTTYPE) {
                    opc = O_LAPPEND;
                } else if (n->children[0]->type->basetype==LISTTYPE) {
                    opc = O_ADD + 9;
                } else {
                    opc = O_ADD + op_type(n->children[0]->type);
                }
                n->code = concat(n->children[0]->code, n->children[1]->code);
                g = gen(opc, n->children[0]->place, n->children[0]->place, n->children[1]->place);
                n->code = concat(n->code, g);
                break;

            case DECREMENT:
                if (n->children[0]->type->basetype==TABLETYPE && n->children[1]->type->basetype!=TABLETYPE) {
                    opc = O_SUB + 6;
                } else {
                    opc = O_SUB + op_type(n->children[0]->type);
                }
                n->code = concat(n->children[0]->code, n->children[1]->code);
                g = gen(opc, n->children[0]->place, n->children[0]->place, n->children[1]->place);
                n->code = concat(n->code, g);
                break;

            case DIVISION:
                opc = O_DIV + op_type(n->children[0]->type);
                n->code = concat(n->children[0]->code, n->children[1]->code);
                n->place = newtemp();
                g = gen(opc, n->place, n->children[0]->place, n->children[1]->place);
                n->code = concat(n->code, g);
                break;

            case MULTIPLICATION:
                opc = O_MUL + op_type(n->children[0]->type);
                n->code = concat(n->children[0]->code, n->children[1]->code);
                n->place = newtemp();
                g = gen(opc, n->place, n->children[0]->place, n->children[1]->place);
                n->code = concat(n->code, g);
                break;

            case UNARY_MINUS:
                opc = O_NEG + op_type(n->children[0]->type);
                n->code = n->children[0]->code;
                n->place = newtemp();
                g = gen(opc, n->place, n->children[0]->place, NULL);
                n->code = concat(n->code, g);
                break;

            case EQUAL:
                opc = O_EQ + op_type(n->children[0]->type);
                n->code = concat(n->children[0]->code, n->children[1]->code);
                n->place = newtemp();
                g = gen(opc, n->place, n->children[0]->place, n->children[1]->place);
                n->code = concat(n->code, g);
                break;

            case LESS_THAN:
                opc = O_LT + op_type(n->children[0]->type);
                n->code = concat(n->children[0]->code, n->children[1]->code);
                n->place = newtemp();
                g = gen(opc, n->place, n->children[0]->place, n->children[1]->place);
                n->code = concat(n->code, g);
                break;

            case GREATER_THAN:
                opc = O_GT + op_type(n->children[0]->type);
                n->code = concat(n->children[0]->code, n->children[1]->code);
                n->place = newtemp();
                g = gen(opc, n->place, n->children[0]->place, n->children[1]->place);
                n->code = concat(n->code, g);
                break;

            case LESS_THAN_EQUAL:
                n->code = concat(n->children[0]->code, n->children[1]->code);
                n->place = newtemp();
                t1 = newtemp();
                opc = O_LT + op_type(n->children[0]->type);
                n->code = concat(n->code, gen(opc, t1, n->children[0]->place, n->children[1]->place));
                t2 = newtemp();
                opc = O_EQ + op_type(n->children[0]->type);
                n->code = concat(n->code, gen(opc, t2, n->children[0]->place, n->children[1]->place));
                n->code = concat(n->code, gen(O_OR, n->place, t1, t2));
                break;

            case GREATER_THAN_EQUAL:
                n->code = concat(n->children[0]->code, n->children[1]->code);
                n->place = newtemp();
                t1 = newtemp();
                opc = O_GT + op_type(n->children[0]->type);
                n->code = concat(n->code, gen(opc, t1, n->children[0]->place, n->children[1]->place));
                t2 = newtemp();
                opc = O_EQ + op_type(n->children[0]->type);
                n->code = concat(n->code, gen(opc, t2, n->children[0]->place, n->children[1]->place));
                n->code = concat(n->code, gen(O_OR, n->place, t1, t2));
                break;

            case NOT_EQUAL:
                opc = O_EQ + op_type(n->children[0]->type);
                n->code = concat(n->children[0]->code, n->children[1]->code);
                n->place = newtemp();
                n->code = concat(n->code, gen(opc, n->place, n->children[0]->place, n->children[1]->place));
                n->code = append(n->code, gen(O_NOT, n->place, n->place, NULL));
                break;

            case UNARY_DI:
                n->place = newtemp();
                n->code = n->children[0]->code;
                // rand()%n + 1
                n->code = concat(n->code, gen(O_RAND, n->place, NULL, NULL));
                n->code = append(n->code, gen(O_MOD, n->place, n->place, n->children[0]->place));
                n->code = append(n->code, gen(O_ADD, n->place, newconst(1), n->place));
                break;

            case BIN_DIE_ROLL:
                // This is pretty long.... there's probably a better way to do this
                n->place = newtemp();
                n->code = n->children[0]->code;
                n->code = append(n->code, n->children[1]->code);
                t1 = newtemp(); t2 = newtemp(); t3 = newtemp();
                l1 = newlabel(); l2 = newlabel();
                // counter = children[0]
                // accumulator = 0
                // while (!counter) {
                //     counter--;
                //     accumulator += rand()
                // }
                // accumulator % children[1]
                // accumulator += children[0] // since we want from 1 to n, not 0 to n
                n->code = append(n->code, gen(O_ASN, t1, n->children[0]->place, NULL));
                n->code = append(n->code, gen(O_ASN, t3, newconst(0), NULL));
                n->code = append(n->code, gen(D_LABEL, l1, NULL, NULL));
                n->code = append(n->code, gen(O_BNIF, t1, l2, NULL));
                n->code = append(n->code, gen(O_SUB, t1, t1, newconst(1)));
                n->code = append(n->code, gen(O_RAND, t2, NULL, NULL));
                n->code = append(n->code, gen(O_ADD, t3, t3, t2));
                n->code = append(n->code, gen(O_GOTO, l1, NULL, NULL));
                n->code = append(n->code, gen(D_LABEL, l2, NULL, NULL));
                n->code = append(n->code, gen(O_MOD, t3, t3, n->children[1]->place));
                n->code = append(n->code, gen(O_ADD, t3, t3, n->children[0]->place));
                n->place = t3;
                break;

            case MODULUS:
                n->place = newtemp();
                n->code = n->children[0]->code;
                n->code = append(n->code, n->children[1]->code);
                n->code = append(n->code, gen(O_MOD, n->place, n->children[0]->place, n->children[1]->place));
                break;

            case SWAPPERATOR:
                temp = newtemp();
                    g = gen(O_ASN, temp, n->children[1]->place, NULL);
                    nc = n;
                    while (nc->children[0]->prodrule == MULTI_SWAP) {
                        g = concat(g, gen(O_ASN, nc->children[1]->place, nc->children[0]->children[1]->place, NULL));
                        nc = nc->children[0];
                    }
                    g = concat(g, gen(O_ASN, nc->children[1]->place, nc->children[0]->place, NULL));
                    g = concat(g, gen(O_ASN, nc->children[0]->place, temp, NULL));
                    n->code = g;
                break;

            case UNARY_NOT:
                n->code = n->children[0]->code;
                n->place = newtemp();
                n->code = concat(n->children[0]->code, gen(O_NOT, newtemp(), n->children[0]->place, NULL));
                break;

            case UNARY_SIZE:
                opc = O_SIZE + op_type(n->children[0]->type);
                n->code = copylist(n->children[0]->code);
                n->place = newtemp();
                g = gen(opc, n->place, n->children[0]->place, NULL);
                n->code = concat(n->code, g);
                break;

            case SPLICE:
                n->code = concat(n->children[0]->code, n->children[1]->code);
                n->code = append(n->code, n->children[2]->code);
                n->place = newtemp();
                n->code = append(n->code, gen(O_LSTRIP, n->place, n->children[0]->place, n->children[1]->place));
                n->code = append(n->code, gen(O_RSTRIP, n->place, n->place, n->children[2]->place));
                break;

            case STRINGCATLIST:
            case IMPLICIT_STRCAT:
                opc = O_ADD + op_type(n->children[0]->type);
                n->code = concat(n->children[0]->code, n->children[1]->code);
                n->place = newtemp();
                g = gen(opc, n->place, n->children[0]->place, n->children[1]->place);
                n->code = concat(n->code, g);
                break;
        // End Basic Operators
        ///////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////
        // Control Flow
            case FORSTATEMENT:
                t1 = newlabel();
                t2 = newlabel();
                n->code = n->children[0] ? n->children[0]->code : n->code;
                n->code = append(n->code, gen(D_LABEL, t1, NULL, NULL));
                n->code = n->children[1] ? append(n->code, n->children[1]->code) : n->code;
                n->code = n->children[1] ? append(n->code, gen(O_BNIF, n->children[1]->place, t2, NULL)) : n->code;
                n->code = n->children[3] ? append(n->code, n->children[3]->code) : n->code;
                n->code = n->children[2] ? append(n->code, n->children[2]->code) : n->code;
                n->code = append(n->code, gen(O_GOTO, t1, NULL, NULL));
                n->code = append(n->code, gen(D_LABEL, t2, NULL, NULL));
                break;

            case IFSTATEMENT:
            case TRAILINGELSEIF:
                t1 = newlabel();
                n->code = n->children[0]->code;
                n->code = append(n->code, gen(O_BNIF, n->children[0]->place, t1, NULL));
                n->code = n->children[1] ? append(n->code, n->children[1]->code) : n->code;
                n->code = append(n->code, gen(D_LABEL, t1, NULL, NULL));
                break;

            case IFELSELIST:
            case IFELSESTATEMENT:
                t1 = newlabel();
                t2 = newlabel();
                n->code = n->children[0]->code;
                n->code = append(n->code, gen(O_BNIF, n->children[0]->place, t1, NULL));
                n->code = n->children[1] ? append(n->code, n->children[1]->code) : n->code;
                n->code = append(n->code, gen(O_GOTO, t2, NULL, NULL));
                n->code = append(n->code, gen(D_LABEL, t1, NULL, NULL));
                n->code = n->children[2] ? append(n->code, n->children[2]->code) : n->code;
                n->code = append(n->code, gen(D_LABEL, t2, NULL, NULL));
                break;

            case TRAILINGELSE:
                if (n->children[0]) {
                    n->place = n->children[0]->place;
                    n->code = n->children[0]->code;
                }
                break;

            case WHILESTATEMENT:
                t1 = newlabel();
                t2 = newlabel();
                n->code = gen(D_LABEL, t1, NULL, NULL);
                n->code = append(n->code, n->children[0]->code);
                n->code = append(n->code, gen(O_BNIF, n->children[0]->place, t2, NULL));
                n->code = n->children[1] ? append(n->code, n->children[1]->code) : n->code;
                n->code = append(n->code, gen(O_GOTO, t1, NULL, NULL));
                n->code = append(n->code, gen(D_LABEL, t2, NULL, NULL));
                break;
        // End Control Flow
        ///////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////
        // Array/Table Stuff
            case TABLELITERAL:
            case EMPTYTABLE:
                n->place = newtemp();
                n->code = get_table_code(n->children[0], n->place);
                n->code = concat(gen(O_TABLE, n->place, newconst(0), NULL), n->code);
                n->code = n->children[0] ? concat(n->children[0]->code, n->code) : n->code;
                break;

            case ARRAYLITERAL:
                n->place = newtemp();
                n->code = get_list_code(n->children[0], n->place);
                temp_int = get_list_size(n->children[0]);
                g = gen(O_LIST, n->place, newconst(temp_int), newconst(0));
                n->code = append(g, n->code);
                n->code = n->children[0] ? concat(n->children[0]->code, n->code) : n->code;
                break;
        ///////////////////////////////////////////////////////////////////////
        // End Array/Table Stuff

        ///////////////////////////////////////////////////////////////////////
        // Function stuff
            case FUNCTIONDEF:
                n->code = n->children[3]->code;
                n->place = newlabel();
                /* ste = get_entry(current, n->children[1]); */
                /* temp_int = ste->type->u.m.parameters ? ste->type->u.m.parameters->size : 0; */
                temp_int = current->scope->u.m.parameters ? current->scope->u.m.parameters->size : 0;
                g = gen_proc(D_PROC, n->children[1]->leaf->text, temp_int, current->size);
                n->code = concat(g, n->code);
                n->code = concat(n->code, gen(D_END, NULL, NULL, NULL));
                break;

            case READ_CALL:
                opc = O_CALL;
                n->place = newtemp();
                g = gen_call(opc, "read", 0, n->place);
                n->code = concat(n->code, g);
                break;

            case WRITE_CALL:
                opc = O_CALL;
                /* // Need a better way to report error */
                /* if (n->children[0]->prodrule == WRITE_ARGLIST) { */
                /*     semanticerror("Write only takes a single argument", n); */
                /* } */
                n->place = newtemp();
                n->code = concat(n->children[0]->code, get_parms_code(n->children[0]));
                g = gen_call(opc, "write", 1, n->place);
                n->code = concat(n->code, g);
                break;

            case FUNCTIONCALL:
                opc = O_CALL;
                n->place = newtemp();
                n->code = n->children[1] ? concat(n->children[1]->code, get_parms_code(n->children[1])) : NULL;
                temp_int = get_n_args(n->children[1]);
                g = gen_call(opc, n->children[0]->leaf->text, temp_int, n->place);
                n->code = concat(n->code, g);
                break;

            case RETURN_STATEMENT:
                opc = O_RET;
                if (n->children[0] == NULL) {
                    n->code = emptylist();
                    g = gen(opc, NULL, NULL, NULL);
                } else {
                    n->code = n->children[0]->code;
                    n->place = n->children[0]->place;
                    g = gen(opc, n->place, NULL, NULL);
                }
                n->code = concat(n->code, g);
                break;
        // End Function stuff
        ///////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////
        // Class Stuff
            case DATAMEMBERACCESS:
                opc = O_MEMBER;
                n->code = concat(n->children[0]->code, n->children[1]->code);
                n->place = newtemp();
                g = gen(opc, n->place, n->children[0]->place, NULL);
                g->sval = n->children[1]->leaf->text;
                n->code = concat(n->code, g);
                break;

            case METHODDEF:
                n->code = n->children[3]->code;
                n->place = newlabel();
                temp_int = current->scope->u.m.parameters ? current->scope->u.m.parameters->size + 1 : 1;
                g = gen_proc(D_PROC, strcatnew(current->parent->scope->u.c.name, n->children[1]->leaf->text, "_"), temp_int, current->size);
                n->code = concat(g, n->code);
                n->code = concat(n->code, gen(D_END, NULL, NULL, NULL));
                break;

            case METHODCALL:
                opc = O_CALL;
                n->place = newtemp();
                n->code = concat(n->children[2]->code, get_parms_code(n->children[2]));
                temp_int = get_n_args(n->children[2]) + 1;
                g = gen_call(opc, strcatnew(n->children[0]->type->u.c.name, n->children[1]->leaf->text, "_"), temp_int, n->place);
                n->code = concat(n->code, gen(O_PARM, n->children[0]->place, NULL, NULL));
                n->code = concat(n->code, g);
                break;

            case CLASSDEF:
                temp_int = size_vars(current);
                temp_int2 = size_methods(current);
                g = gen_proc(D_CLASS, n->children[1]->leaf->text, temp_int, temp_int2);
                n->code = concat(g, n->children[2]->code);
                break;

            case CONSTRUCTOR:
                n->code = n->children[2]->code;
                n->place = newlabel();
                ste = get_entry(current, n->children[0]);
                temp_int = ste->type->u.con.parameters ? ste->type->u.con.parameters->size : 0;
                g = gen_proc(D_PROC, strcatnew(current->parent->scope->u.c.name, n->children[0]->leaf->text, "_"), temp_int, current->size);
                n->code = concat(g, n->code);
                n->code = concat(n->code, gen(D_END, NULL, NULL, NULL));
                break;

            case CONSTRUCTORCALL:
                opc = O_NEW;
                n->place = newtemp();
                n->code = n->children[1] ? concat(n->children[1]->code, get_parms_code(n->children[1])) : NULL;
                temp_int = get_n_args(n->children[1]);
                g = gen_call(opc, n->children[0]->leaf->text, temp_int, NULL);
                n->code = concat(n->code, g);
                break;

            case VARIABLEDECLARATION:
                if (current->scope && current->scope->basetype == CLASS) {
                    n->code = gen(D_FIELD, n->children[1]->place, NULL, NULL);
                }
                break;

            case VARIABLEDECLARATIONLIST:
                if (current->scope->basetype == CLASS) {
                    n->code = gen(D_FIELD, n->children[1]->place, NULL, NULL);
                }
                n->code = concat(n->code, n->children[2]->code);
                break;

            case VARIABLEDECLARATIONLISTCONTENTS:
                if (current->scope->basetype == CLASS) {
                    n->children[0]->code = gen(D_FIELD, n->children[0]->place, NULL, NULL);
                    if (n->children[1]->prodrule == 0) {
                        n->children[1]->code = gen(D_FIELD, n->children[1]->place, NULL, NULL);
                    }
                    n->code = concat(n->children[0]->code, n->children[1]->code);
                    /* n->code = concat(g, gen(D_FIELD, n->children[1]->place, NULL, NULL)); */
                }
                break;
        // EndClass Stuff
        ///////////////////////////////////////////////////////////////////////

        case INT_TO_DOUBLE:
            n->place = newtemp();
            n->code = concat(n->children[0]->code, gen(O_ITOD, n->place, n->children[0]->place, NULL));
            break;

        /* // Not needed */
        /* case TABLEMULTIENTRY: */
        /* case TABLESINGLEENTRY: */
        /* case TABLETYPE: */
        /* case ARRAYCONTENTS: */
        /* case LISTTYPE: */
        /* case DECLARATIONS: */
        /* case ARGLIST: */
        /* case FUNCTIONBODY_NO_DECL: */
        /* case FUNCTIONBODY_W_DECL: */
        /* case CLASSBODYLIST: */
        /* case ARG_DEF_LIST: */
        /* case TOPLEVELLIST: */
        /* case ARGDEFLIST: // Mabye */
        /* case WRITE_ARGLIST: */

        case GROUPED_EXP:
            n->place = n->children[0]->place;
        default:
            /* default is: concatenate our children's code */
            n->code = NULL;
            for(i = 0; i < n->nChildren; i++) {
                if (n->children[i] != NULL)
                n->code = concat(n->code, n->children[i]->code);
            }
    }

    /* Deactivate the current scope */
    switch (n->prodrule) {
        case CLASSDEF:
        case FUNCTIONDEF:
        case METHODDEF:
        case CONSTRUCTOR:
            popscope();
            break;
    }
}
