#ifndef HELPER_H
#define HELPER_H

#include "structs.h"

Tree* allocate_leaf();
Tree* allocate_token(int code);
Tree* allocate_tree(int p, int argc, ...);
char* get_basefile(char* infile);
char* get_types_str(int types);
char* pretty_print_type(TypeInfo* t);
char* strcatnew(char* c1, char* c2, char* delimit);
char* verify_filename(char* input);
int is_class(char* test, char** classes, int numclasses);
int parse_string(char* retval, char* input);
int yyerror(char const *s);
void add_class(Tree* t);
void compile(char* basefile);
void gen_as(char* basefile, struct instr* code);
void gen_o(char* basefile);
void lexerr(char* s, int lineno);
void print_code(char* infile, Tree* root);
void print_tree(Tree* n, int depth);
void semanticerror(char* s, Tree* n);
void semanticerror_prod(char *s, Tree* n);
void typeerror1(int types, TypeInfo* n2, Tree* n);
void typeerror1_alt(TypeInfo* n1, TypeInfo* n2, Tree* n);
void typeerror2(char* name, Tree* n);
void typeerror3(int argno, char* name, TypeInfo* t1, TypeInfo* t2, Tree* n);
void typeerror4(int argno, char* name, Tree* n);
void typeerror_mult(TypeInfo* n1, TypeInfo* n2, Tree* n, int types);

#endif
