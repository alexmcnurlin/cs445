#ifndef ERROR_H
#define ERROR_H


#define LONG_IDENTIFIER_ERROR "Remember, identifier names can only be 12 characters long"
#define NO_VAR_DECLARATION1 "Expected '('. Variable declarations must be before function/class definitions!"
#define NO_VAR_DECLARATION2 "Expected '('. Variable declarations must be before function definitions!"
#define ASSIGNMENT_ERROR "Cannot assign variable during initialization"
#define NO_FUNC_DECLARATION1 "Expected function definitions. Variable/function declarations must be before function definitions!"
#define MISSING_ARGS "Incomplete argument declaration in function header!"
#define SEMI_ERROR "Semicolons not supported as statement enders!"
#define INITIALIZATION_ERROR "Please declare variable before initialization"
#define UNDERSCORE_IDENTIFIER_ERROR "Underscores are not valid in identifiers!"
#define SYMBOL_DEFINED_ERROR "Redeclaration of symbol"
#define SYMBOL_UNDEFINED_ERROR "Symbol has not been declared:"

#define LEXICAL_ERROR 1
#define SYNTAX_ERROR 2
#define SEMANTIC_ERROR 4

#define ARGCHECK if (IS_PROTOTYPE) { yyerror(MISSING_ARGS); }
#define RESET_ARG IS_PROTOTYPE=0;

#endif
