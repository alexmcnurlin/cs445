#ifndef TAC_H
#define TAC_H

#include "structs.h"

/* Regions: */
#define R_GLOBAL 2001 /* can assemble as relative to the pc */
#define R_LOCAL  2002 /* can assemble as relative to the ebp */
#define R_CLASS  2003 /* can assemble as relative to the 'this' register */
#define R_LABEL  2004 /* pseudo-region for labels in the code region */
#define R_CONST  2005 /* pseudo-region for immediate mode constants */
#define R_STRING 2006 /* pseudo-region for string constants */
#define R_DOUBLE 2007
#define R_UNDEF  2008

#define O_ADD      30010
#define O_SUB      30020
#define O_MUL      30030
#define O_DIV      30040
#define O_NEG      30050
#define O_ASN      30060
#define O_ADDR     30070
#define O_LCONT    30080
#define O_SCONT    30090
#define O_GOTO     30100
#define O_BLT      30110
#define O_BLE      30120
#define O_BGT      30130
#define O_BGE      30140
#define O_BEQ      30150
#define O_BNE      30160
#define O_BIF      30170
#define O_BNIF     30180
#define O_PARM     30190
#define O_CALL     30200
#define O_RET      30210
#define O_ITOD     30220
#define O_LT       30230
#define O_GT       30240
#define O_OR       30250
#define O_AND      30260
#define O_EQ       30270
#define O_NEQ      30280
#define O_MEMBER   30290
#define O_NEW      30300
#define O_RAND     30310
#define O_MOD      30320
#define O_NOT      30330
#define O_INDEX    30340
#define O_ADIND    30350
#define O_LAPPEND  30360
#define O_TDEFAULT 30370
#define O_SIZE     30380
#define O_LIST     30390
#define O_TABLE    30400
#define O_LSTRIP   30410
#define O_RSTRIP   30420

/* declarations/pseudo instructions */
#define D_GLOB   30510
#define D_PROC   30520
#define D_LOCAL  30530
#define D_LABEL  30540
#define D_END    30550
#define D_CLASS  30560
#define D_FIELD  30570
#define D_STRING 30580

Address* newconst(int val);
Address* newdouble();
Address* newlabel();
Address* newloc(SymbolTable* st, int region, int size);
Address* newstring();
Address* newtemp();
Instruction* append(Instruction* l1, Instruction* l2);
Instruction* concat(Instruction* l1, Instruction* l2);
Instruction* copylist(Instruction* l);
Instruction* emptylist();
Instruction* gen(int op, Address* a1, Address* a2, Address* a3);
Instruction* gen_call(int op, char* name, int nParams, Address* retloc);
Instruction* gen_proc(int op, char* name, int nargs, int nlocals);
Instruction* get_list_code(Tree* n, Address* dest);
Instruction* get_parms_code(Tree* n);
Instruction* get_table_code(Tree* n, Address* dest);
char* pretty_print_opcode(int opcode);
char* pretty_print_region(int region);
int get_list_size(Tree* n);
int get_n_args(Tree* n);
int op_type(TypeInfo* type);
int size_methods(SymbolTable* st);
int size_vars(SymbolTable* st);

#endif
