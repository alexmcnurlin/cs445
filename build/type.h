#ifndef TYPE_H
#define TYPE_H

#include "structs.h"

extern struct typeinfo integer_type;
extern struct sym_table *foo;

TypeInfo* alcclasstype(Tree* c, SymbolTable *);
TypeInfo* alcconstructortype(Tree* c, SymbolTable* st);
TypeInfo* alcfunctype(Tree* f, SymbolTable* st);
TypeInfo* alcmethodtype(Tree* m, SymbolTable*);
TypeInfo* alctype(int);
TypeInfo* compare_type(TypeInfo* t1, TypeInfo* t2, int types);
TypeInfo* perform_typechecking(Tree* n);
TypeInfo* typecheck(Tree* n, int check);
TypeInfo* typecheck_children(Tree* n, int valid_types);
TypeInfo* typecheck_function(SymbolTable* st, Tree* n, Tree* funcname, Tree* in_params, int functype);
char* typename(TypeInfo* t);
int is_assignable(Tree* n);
void promote_to_double(Tree* n, int i);
int valid_type(int types, TypeInfo* t1);

extern TypeInfo* integer_typeinfo;
extern TypeInfo* string_typeinfo;
extern TypeInfo* double_typeinfo;
extern TypeInfo* bool_typeinfo;

#define M_INT       0b00000001
#define M_DOUBLE    0b00000010
#define M_STRING    0b00000100
#define M_BOOL      0b00001000
#define M_VOID      0b00010000
#define M_CLASS     0b00100000
#define M_LIST      0b01000000
#define M_TABLE     0b10000000
#define M_ANY       0b11111111
#endif
