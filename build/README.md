# cs445

A Lexer for UIdaho CS445

## Compilation

`make`

## Run

`make run`
`make run file=path/to/file`
`./g0`

## Options

Only one option can be used at a time, and the option ***MUST*** be the first argument.

- `-g`: Create parse_tree_simple.dot and parse_tree_full.dot files that can be compiled using graphviz to create a visual parser tree
- `-s`: Print out symbol tables
- `-t`: Print out tree

## Notes on Errors

The compiler will halt on the following and exit with an error code:

- Lexical Error
- Undefinited Variable (semantic error). There may be cases which don't halt. These may end with a segfault.

The compiler will (attempt to) continue on the following, but will return error code at finish:

- Parse Error
- Type Error

Naturally, the attempt to recover from an error may cause more errors that don't actually exist. 
Notably lines with added semicolons will simply be thrown out

Errors will report the file, line number, type of error, and message for the error. This has a few caveats: 

- The line number reported will be the line number of the first token stored in the syntax tree for that production. i.e. multi-line productions may report a line number before the error.
- Type errors involving lists, tables, and classes will only show base type. e.g. an error assigning a list<int> to a list<string> will say 'expected list but got list'

## Extra features

### Visual syntax tree
When you run the program, the files parse_tree_full.dot and parse_tree_simple.dot
can be created to allow the user to generate a png showing the syntax tree.

- `make graph file=<filename>` for a simple syntax tree.
- `make graph G=full file=<filename>` for a syntax tree with all struct contents.

## Unsupported Syntaxes

The following are features that were not implemented due to either time constraints,
or reduce/reduce errors, or were explicitly said to not be part of the grammar (but were present in example files).

- Semicolons
  * The grammar was designed to not need semicolons. If you use one outside a for loop header, an error will be thrown
	* `int main(int i) {i = 1;} // Syntax error`
- Declarations in middle of scope
  * All declarations  must happen at the top of their respective scopes.
- Implicit string concatenation outside write
	* `"hello" "world"   // Not allowed`
	* `write("hello" "world") // Allowed`
	* `write("hello" get_name()) // Allowed`
	* `"hello" + "world" // Explicit concatenation is allowed`
- Bare expressions
	* `"int main() {1 + 2} // Syntax error`
	* `"int main() {i = 1 + 2} // Allowed`
- Function Call as an operator
  * `some_func()() // Not allowed`
  * `f = some_func() \n f() // Allowed`

