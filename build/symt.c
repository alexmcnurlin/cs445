/* Borrowed from */
/* Modified by Alex McNurlin for University of Idaho CS445 HW3 */
/* symt.c */


/* The functions in this file maintain a hash table mapping strings to */
/* symbol table entries. */
#include "errors.h"
#include "helper.h"
#include "parser.tab.h"
#include "prodrules.h"
#include "structs.h"
#include "symt.h"
#include "tac.h"
#include "type.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern int errors;
extern int TYPEERROR;

SymbolTable* current = NULL;
SymbolTable* global = NULL;
TypeInfo* current_type = NULL;
int tree_break = 0; // A flag to tell us whether we should halt the tree traversal and exit

char *alloc(int n);       /* calloc + check for NULL */

// default size of symbol tables
#define SYMT_SIZE 40


/* new_st - construct symbol (hash) table. */
/* Allocate space first for the structure, then */
/* for the array of buckets. */
SymbolTable* new_st(int nbuckets) {
    SymbolTable* rv;
    rv = (SymbolTable*) alloc(sizeof(struct sym_table));
    rv->tbl = (struct sym_entry **)
    alloc((unsigned int)(nbuckets * sizeof(struct sym_entry *)));
    rv->nBuckets = nbuckets;
    rv->nEntries = 0;
    return rv;
}


/* delete_st - destruct symbol table. */
void delete_st(SymbolTable* st) {
    SymbolTableEntry *se, *se1;
    int h;

    for (h = 0; h < st->nBuckets; ++h) {
        for (se = st->tbl[h]; se != NULL; se = se1) {
            se1 = se->next;
            free((char *)se->s); /* strings in the table were all strdup'ed. */
            free((char *)se);
        }
        st->tbl[h] = NULL;
    }
    free((char *)st->tbl);
    free((char *)st);
}


/* Compute hash value. */
int hash(SymbolTable* st, char *s) {
    register int h = 0;
    register char c;
    while ((c = *s++)) {
        h += c & 0377;
        h *= 37;
    }
    if (h < 0) h = -h;
    return h % st->nBuckets;
}


/* Insert a symbol into a symbol table. */
int insert_sym(SymbolTable* st, Tree *s, TypeInfo* t) {
    /* printf("Inserting symbol %s into table at %p\n", s, st); */
    int h;
    struct sym_entry *se;

    h = hash(st, s->leaf->text);
    for (se = st->tbl[h]; se != NULL; se = se->next) {
        if (!strcmp(s->leaf->text, se->s)) {
            /* A copy of the string is already in the table. */
            semanticerror(SYMBOL_DEFINED_ERROR, s);
            return -1;
        }
    }

    /* The string is not in the table. Add the copy from the */
    /* buffer to the table. */
    se = (SymbolTableEntry*)alloc((unsigned int) sizeof (struct sym_entry));
    se->next = st->tbl[h];
    se->table = st;
    st->tbl[h] = se;
    se->s = strdup(s->leaf->text);
    se->type = t;
    if (se->type->basetype != FUNCTIONDEF || se->type->basetype != METHODDEF) {
        int region;
        if (st == global) {
            region = R_GLOBAL;
        } else if (st->scope->basetype == CLASS) {
            region = R_CLASS;
        } else {
            region = R_LOCAL;
        }
        se->place = newloc(current, region, 8);
        se->size = 8;
    }
    st->nEntries++;
    return h;
}


/* lookup_st - search the symbol table for a given symbol, return its entry. */
SymbolTableEntry* lookup_st(SymbolTable* st, char *s) {
    int h;
    SymbolTableEntry* se;

    h = hash(st, s);
    for (se = st->tbl[h]; se != NULL; se = se->next) {
        if (!strcmp(s, se->s)) {
            /* Return a pointer to the symbol table entry. */
            return se;
        }
    }
    return NULL;
}


/* Search the symbol table and its parents for a given symbol, return its entry */
SymbolTableEntry* get_entry(SymbolTable* st, Tree* s) {
    SymbolTableEntry* res;
    do {
        res = lookup_st(st, s->leaf->text);
        if (res != NULL) {
            return res;
        }
    } while ((st = st->parent) != NULL);
    return res;
}


char* alloc(int n) {
    char *a = calloc(n, sizeof(char));
    if (a == NULL) {
        fprintf(stderr, "alloc(%d): out of memory\n", (int)n);
        exit(-1);
    }
    return a;
}


/* Create a new symbol table */
/* Set the global `current` to the new symbol table */
/* Add the leaf stored in `s` to the symbol table */
void enter_newscope(Tree* n, Tree *s, int typ) {
    SymbolTable* new = new_st(SYMT_SIZE);
    TypeInfo* t;
    switch(typ) {
        case CLASS:
            t = alcclasstype(n->children[1], new);
            break;
        case FUNCTIONDEF:
            t = alcfunctype(n, new);
            break;
        case METHODDEF:
            t = alcmethodtype(n, new);
            break;
        case CONSTRUCTOR:
            t = alcconstructortype(n, new);
            break;
    }
    new->scope = t;
    insert_sym(current, s, t);
    pushscope(new);
}

// Return the symboltable associated with the symbol s
SymbolTable* get_scope(char* s) {
    SymbolTableEntry* ste = lookup_st(current, s);
    if (!ste) { return NULL; }
    switch (ste->type->basetype) {
        case CLASS:
            return ste->type->u.c.st;
        case FUNCTIONDEF: 
        case METHODDEF:
            return ste->type->u.m.st;
        case CONSTRUCTOR:
            return ste->type->u.con.st;
    }
    return NULL;
}

// Iterate through the syntax tree to create/populate symbol tables
void populate_symboltables(Tree* n) {
    int i;
    if (n == NULL || tree_break) {
        return;
    }

    /* If the global symbol table doesn't exist yet, create it */
    if (current == NULL) {
        current = new_st(SYMT_SIZE);
        global = current;
    }

    /* pre-order activity */
    TypeInfo* type;
    Tree* p = n;
    /* SymbolTableEntry* ste; */
    switch (n->prodrule) {
        // Create new symbol tables
        case CLASSDEF:
            enter_newscope(n, n->children[1], CLASS);
            n->table = current;
            break;
        case FUNCTIONDEF:
            enter_newscope(n, n->children[1], FUNCTIONDEF);
            n->table = current;
            break;
        case METHODDEF:
            enter_newscope(n, n->children[1], METHODDEF);
            n->table = current;
            break;
        case CONSTRUCTOR:
            enter_newscope(n, n->children[0], CONSTRUCTOR);
            n->table = current;
            break;

        // Insert symbols
        case ARG_DEF_LIST:
            insert_sym(current, n->children[1],  get_type(n->children[0]));
            break;
        case VARIABLEDECLARATION:
            insert_sym(current, n->children[1],  get_type(n->children[0]));
            break;
        case VARIABLEDECLARATIONLIST:
            type = get_type(n->children[0]);
            insert_sym(current, n->children[1], type);
            // Traverse the list of declared variables
            p = n->children[2];
            while (p->leaf == NULL) {
                insert_sym(current, p->children[0], type);
                p = p->children[1];
            }
            insert_sym(current, p, type);
            break;
    }

    // Recurse
    switch (n->prodrule) {
        case DATAMEMBERACCESS:
            /* Ignore method/member names. Those will be checked with the types */
            populate_symboltables(n->children[0]);
            break;
        case METHODCALL:
            populate_symboltables(n->children[0]);
            populate_symboltables(n->children[2]);
            break;
        default:
            /* visit children */
            for (i=0; i<n->nChildren; i++) {
                populate_symboltables(n->children[i]);
            }
    }
    if (tree_break) return; 

    /* Deactivate the current scope */
    switch (n->prodrule) {
        case CLASSDEF:
        case FUNCTIONDEF:
        case METHODDEF:
        case CONSTRUCTOR:
            popscope();
            break;
    }
}


// Perform semantic analysis (type checking, variables declared, ect)
void semantic_analysis(Tree* n) {
    int i;
    if (n == NULL || tree_break) {
        return;
    }

    /* pre-order activity */
    SymbolTableEntry* ste;
    switch (n->prodrule) {
        // Create new symbol tables
        case CLASSDEF:
            current = get_scope(n->children[1]->leaf->text);
            ste = get_entry(current, n->children[1]);
            if (!ste || ste->type->basetype != CONSTRUCTOR) {
                semanticerror("Constructor not found in class ", n->children[1]);
            }
            break;
        case FUNCTIONDEF:
            current = get_scope(n->children[1]->leaf->text);
            break;
        case METHODDEF:
            current = get_scope(n->children[1]->leaf->text);
            break;
        case CONSTRUCTOR:
            current = get_scope(n->children[0]->leaf->text);
            break;

        case 0:
            switch (n->leaf->category) {
                case IDENT:
                    // Check if symbol is undefined
                    ste = get_entry(current, n);
                    if (ste == NULL) {
                        semanticerror(SYMBOL_UNDEFINED_ERROR, n);
                        tree_break = 1;
                    }
                    break;
            }
            break;
    }

    // Recurse
    switch (n->prodrule) {
        case DATAMEMBERACCESS:
            /* Ignore method/member names. Those will be checked with the types */
            semantic_analysis(n->children[0]);
            break;
        case METHODCALL:
            semantic_analysis(n->children[0]);
            semantic_analysis(n->children[2]);
            break;
        default:
            /* visit children */
            for (i=0; i<n->nChildren; i++) {
                semantic_analysis(n->children[i]);
            }
    }
    if (tree_break) return; 

    n->lineno = get_lineno(n);
    n->type = perform_typechecking(n);

    /* Deactivate the current scope */
    switch (n->prodrule) {
        case CLASSDEF:
        case FUNCTIONDEF:
        case METHODDEF:
        case CONSTRUCTOR:
            popscope();
            break;
    }
}


void print_st(SymbolTable* st, int level) {
    int i;
    SymbolTableEntry* ste;
    if (st == NULL) return;
    for (i=0;i<st->nBuckets;i++) {
        for (ste = st->tbl[i]; ste; ste=ste->next) {
            printf("%*s", 4*level, " ");
            switch (ste->type->basetype) {
                case CLASS:
                    // I use the same typeinfo object for a class and an instance of the class
                    // so we check if the name of the symbol is the same as its class
                    if (strcmp(ste->s, ste->type->u.c.name) == 0) {
                        printf("Class %s\n", ste->s);
                        print_st(ste->type->u.c.st, level+1);
                    } else {
                        printf("%s %s\n", ste->type->u.c.name, ste->s);
                    }
                    break;
                case FUNCTIONDEF:
                    printf("%i %s <- ", ste->type->u.m.returntype->basetype, ste->type->u.m.name);
                    print_param_list(ste->type->u.m.parameters);
                    printf("\n");
                    print_st(ste->type->u.m.st, level+1);
                    break;
                case METHODDEF:
                    printf("%i %s <- ", ste->type->u.m.returntype->basetype, ste->type->u.m.name);
                    print_param_list(ste->type->u.m.parameters);
                    printf("\n");
                    print_st(ste->type->u.m.st, level+1);
                    break;
                case CONSTRUCTOR:
                    printf("Constructor %s <- ", ste->s);
                    print_param_list(ste->type->u.con.parameters);
                    printf("\n");
                    print_st(ste->type->u.con.st, level+1);
                    break;
                default:
                    printf("%i %s\n", ste->type->basetype, ste->s);
                    break;
            }
        }
    }
}


void print_param_list(ParamList* p) {
    if (p == NULL) {
        printf("(void)");
    }

    while (p!= NULL) {
        printf("%s:%i, ", p->name, p->type->basetype);
        p = p->next;
    }
}


TypeInfo* get_type(Tree* type) {
    TypeInfo* t;
    SymbolTableEntry* ste;
    switch (type->prodrule) {
        case 0:
            switch(type->leaf->category) {
                case INT:
                    t = alctype(INT);
                    break;
                case DOUBLE:
                    t = alctype(DOUBLE);
                    break;
                case STRING:
                    t = alctype(STRING);
                    break;
                case BOOLEAN:
                    t = alctype(BOOLEAN);
                    break;
                case VOID:
                    t = alctype(NULLLITERAL);
                    break;
                case CLASSNAME:
                    ste = get_entry(current, type);
                    if (ste->type->basetype == CONSTRUCTOR) {
                        // Fix a bug where a method returning a class type
                        // would get the constructor type
                        t = ste->table->scope;
                    } else {
                        t = ste->type;
                    }
                    break;
            }
            break;

        case LISTTYPE:
            t = alctype(LISTTYPE);
            t->u.a.elemtype = type->children[0] != NULL ? get_type(type->children[0]) : integer_typeinfo;
            break;

        case TABLETYPE:
            t = alctype(TABLETYPE);
            t->u.t.keytype = type->children[0] != NULL ? get_type(type->children[0]) : integer_typeinfo;
            t->u.t.valtype = type->children[1] != NULL ? get_type(type->children[1]) : string_typeinfo;
            break;

        default:
            printf("Whoops\n");
    }
    return t;
}


ParamList* get_parameters(Tree* params) {
    if (params == NULL) {return NULL;}
    ParamList *head, *tail;
    head = calloc(1, sizeof(ParamList));
    if (params->prodrule != ARGDEFLIST) {
        head->name = params->children[1]->leaf->text;
        head->type = get_type(params->children[0]);
        head->next = NULL;
        head->size = 1;
        return head;
    }

    head->type = get_type(params->children[0]->children[0]);
    head->name = params->children[0]->children[1]->leaf->text;
    head->next = NULL;
    head->size = 1;
    tail = head;
    while (params->children[1]->prodrule == ARGDEFLIST) {
        tail->next = calloc(1, sizeof(ParamList));
        tail = tail->next;
        tail->name = params->children[0]->children[1]->leaf->text;
        tail->type = get_type(params->children[0]->children[0]);
        head->size += 1;
        params = params->children[1];
    }
    tail->next = calloc(1, sizeof(ParamList));
    tail = tail->next;
    tail->name = params->children[1]->children[1]->leaf->text;
    tail->type = get_type(params->children[1]->children[0]);
    tail->next = NULL;
    head->size += 1;
    return head;
}


// Return the approximate line number of the tree n
// This is done by getting the linenumber of the first child where the linenumber
//     is non-negative.
// If the linenumber cannot be found, set it to -1
// A line number of -1 means all children are NULL, so we cannot deduce the line
int get_lineno(Tree* n) {
    int i, retval;
    if (!n->prodrule) {
        return n->leaf->lineno;
    } else {
        for (i = 0; i < n->nChildren; i++) {
            if (n->children[i] != NULL) {                  // If child exists
                if (!n->children[i]->prodrule) {           // If child is a leaf
                    retval = n->children[i]->leaf->lineno;
                    if (retval != -1) return retval;
                } else {                                   // If child is not leaf
                    retval = n->children[i]->lineno;
                    if (retval != -1) return retval;
                }
            }
        }
        return -1;  // If nothing is found, say line number is -1
    }
}
