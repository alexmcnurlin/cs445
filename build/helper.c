#ifndef HELPER_C
#define HELPER_C

#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <time.h>

#include "tac.h"
#include "errors.h"
#include "helper.h"
#include "main.h"
#include "parser.tab.h"
#include "prodrules.h"
#include "structs.h"
#include "type.h"

extern FILE* yyin;
extern char* yytext;
extern Token* yytoken; extern int errors; extern int line_num;
extern int yylex();
extern int yylineno;
extern char* pretty_print_name(int prodrule);
extern char* filename;
int NUMCLASSES = 0;
int FNAME_END = 0;
char** CLASSES;
extern int NSTRINGS;
extern char** STRINGS_LIST;
extern int NDOUBLES;
extern double* DOUBLES_LIST;

int ERROR_TYPES = 0;

// Taken from the clex.l example from the website
// http://www2.cs.uidaho.edu/~jeffery/courses/445/clex.l
void lexerr(char *s, int lineno) {
    fprintf(stderr, "LexicalError: %s:%i\n\t%s around text '%s'\n", filename, yylineno, s, yytext);
    ERROR_TYPES |= LEXICAL_ERROR;
    errors++;
}


int yyerror(char const *s) {
    if (strcmp(s, "syntax error") == 0) {
        fprintf(stderr, "SyntaxError: %s:%i\n\t%s around text '%s'\n", filename, yylineno, s, yytext);
    } else {
        fprintf(stderr, "SyntaxError: %s:%i\n\t%s\n", filename, yylineno, s);
    }
    ERROR_TYPES |= SYNTAX_ERROR;
    errors++;
    return 0;
}


void semanticerror(char *s, Tree* n) {
    fprintf(stderr, "SemanticError: %s:%i\n\t%s '%s'\n", n->leaf->filename, n->leaf->lineno, s, n->leaf->text);
    ERROR_TYPES |= SEMANTIC_ERROR;
    errors++;
}


void semanticerror_prod(char *s, Tree* n) {
    fprintf(stderr, "SemanticError: %s:%i\n\t%s\n", n->filename, n->lineno, s);
    ERROR_TYPES |= SEMANTIC_ERROR;
    errors++;
}


void typeerror1(int types, TypeInfo* n2, Tree* n) {
    char* types_string = get_types_str(types);
    fprintf(stderr, "TypeError: %s:%i\n\tInvalid type! Expected %s but got %s\n", n->filename, n->lineno, types_string, pretty_print_type(n2));
    ERROR_TYPES |= SEMANTIC_ERROR;
    errors++;
    free(types_string);
}


void typeerror1_alt(TypeInfo* n1, TypeInfo* n2, Tree* n) {
    fprintf(stderr, "TypeError: %s:%i\n\tInvalid type! Expected %s but got %s\n", n->filename, n->lineno, pretty_print_type(n1), pretty_print_type(n2));
    ERROR_TYPES |= SEMANTIC_ERROR;
    errors++;
}


void typeerror2(char* name, Tree* n) {
    fprintf(stderr, "TypeError: %s:%i\n\tFunction %s takes no arguments\n", n->filename, n->lineno, name);
    ERROR_TYPES |= SEMANTIC_ERROR;
    errors++;
}


void typeerror3(int argno, char* name, TypeInfo* t1, TypeInfo* t2, Tree* n) {
    fprintf(stderr, "TypeError: %s:%i\n\tArgument %i of %s expected %s but got %s\n", n->filename, n->lineno, argno, name, pretty_print_type(t1), pretty_print_type(t2));
    ERROR_TYPES |= SEMANTIC_ERROR;
    errors++;
}


void typeerror4(int argno, char* name, Tree* n) {
    fprintf(stderr, "TypeError: %s:%i\n\tFuncion %s received extra arguments\n", n->filename, n->lineno, name);
    ERROR_TYPES |= SEMANTIC_ERROR;
    errors++;
}


void typeerror_mult(TypeInfo* n1, TypeInfo* n2, Tree* n, int types) {
    if (types == M_ANY) {
        fprintf(stderr, "TypeError: %s:%i\n\tInvalid type! Expected %s but got %s\n", n->filename, n->lineno, pretty_print_type(n1), pretty_print_type(n2));
    } else  {
        char* types_string = get_types_str(types);
        fprintf(stderr, "TypeError: %s:%i\n\tInvalid type! Expected one of %s but got %s and %s\n", n->filename, n->lineno, types_string, pretty_print_type(n1), pretty_print_type(n2));
        free(types_string);
    }
    ERROR_TYPES |= SEMANTIC_ERROR;
    errors++;
}


// Allocates new string
// Given the bitmask `types`, construct a string representation of the types
// 0b00100011 => "class, int, or double"
char* get_types_str(int types) {
    char* types_string = calloc(49, sizeof(char)); // The max size of the string
    types_string[0] = '\0';
    while (types) {
        if (types & M_INT) {
            strcat(types_string, "int");
            types &= ~M_INT;
        } else if (types & 0b00000010) {
            strcat(types_string, "double");
            types &= ~M_DOUBLE;
        } else if (types & M_STRING) {
            strcat(types_string, "string");
            types &= ~M_STRING;
        } else if (types & M_BOOL) {
            strcat(types_string, "boolean");
            types &= ~M_BOOL;
        } else if (types & M_VOID) {
            strcat(types_string, "void");
            types &= ~M_VOID;
        } else if (types & M_CLASS) {
            strcat(types_string, "class");
            types &= ~M_CLASS;
        } else if (types & M_LIST) {
            strcat(types_string, "list");
            types &= ~M_LIST;
        } else if (types & M_TABLE) {
            strcat(types_string, "table");
            types &= ~M_TABLE;
        }

        if (!types) {
        } else if (types && !(types & (types-1))) {
        //         ^^^^ Black magic ^^^^^^^^^^^^  
        //  https://stackoverflow.com/questions/51094594/how-to-check-if-exactly-one-bit-is-set-in-an-int
        //  Checks if only one bit is set
            strcat(types_string, ", or ");
        } else {
            strcat(types_string, ", ");
        }
    }
    return types_string;
}


char* pretty_print_type(TypeInfo* t) {
    if (t == NULL) {
        return "nothing";
    }
    switch (t->basetype) {
        case INT:         return "int";
        case STRING:      return "string";
        case DOUBLE:      return "double";
        case BOOLEAN:     return "boolean";
        case NULLLITERAL: return "void";
        case CLASSNAME:   return t->u.c.name;
        case METHODDEF:   return pretty_print_type(t->u.m.returntype);
        case CONSTRUCTOR: return pretty_print_type(t->u.con.returntype);
        case LISTTYPE:    return "list";
        case TABLETYPE:   return "table";
        case CLASS:       return t->u.c.name;
        default:
            return "Unknown type";
    }
}


// Removes surrounding quotes and replace escape characters in the given string
// overwrites retval. Return 1 on error, 0 on sucess
int parse_string(char* retval, char* input) {
    int i = 1;
    int j = 0;
    char ch;
    while (input[i] != '\0') {
        if (input[i] == 92) { // Backslash
            switch (input[++i]) {
                case 92: // '\\'
                    ch = 92;
                    break;
                case 'n': // '\n'
                    ch = 10;
                    break;
                case 't': // '\t'
                    ch = 9;
                    break;
                case '"': // '\"''
                    ch = 34;
                    break;
                default: // If we get an invalid character
                    return 1;
            }
        } else {
            ch = input[i];
        }
        retval[j] = ch;
        i++;
        j++;
    }
    retval[j-1] = '\0';
    return 0;
}


// Verify that the filename is a g0 file.
// if no extension is present, return a copy of the input string with .g0 appended to the end
// if a filename is given with a different extension, print a message to stderr and return the input string
// if a filename with a g0 extension is given, return the input string.
char* verify_filename(char* input) {
    char* substr;
    substr = strstr(input, ".g0");
    if (strcmp(input, "/dev/stdin") == 0) {
        return input;
    }
    if (substr != NULL && (strlen(substr) == 3)) {
        return input;
    }
    substr = strstr(input, ".");
    char* old = NULL; // if no substr is found, old will always NULL
    while (substr != NULL) {
        old = substr;
        substr = substr + 1;
        substr = strstr(substr, ".");
    }
    if (old == NULL) {
        char* new_string = calloc(strlen(input)+3, sizeof(char));
        strcpy(new_string, input);
        // find end of string
        int i = 0;
        while (new_string[i] != '\0') {
            i++;
        }
        new_string[i] = '.';
        new_string[i+1] = 'g';
        new_string[i+2] = '0';
        new_string[i+3] = '\0';
        return new_string;
    } else {
        fprintf(stderr, "The file extension on %s is not recognized\n", input);
        return input;
    }
}


Tree* allocate_leaf(int code) {
    // Create token to add to list
    Token* tok = calloc(1, sizeof(Token));
    tok->text = strdup(yytext);
    tok->category = code;
    tok->lineno = line_num;
    tok->filename = filename;
    if (code == STRINGLITERAL) {
        // Parse escape sequences
        char* str;
        str = strdup(tok->text);
        int status = parse_string(str, tok->text);
        if (status == 1) {
            lexerr(yytext, yylineno);
            free(tok->text);
            free(tok);
            return NULL;
        }
        tok->sval = str;
    } else if (code == INTLITERAL) {
        tok->ival = atoi(tok->text);
    } else if (code == REALLITERAL) {
        tok->dval = atof(tok->text);
    } else if (code == TRUE) {
        tok->ival = 1;
    } else if (code == FALSE) {
        tok->ival = 0;
    }
    Tree* t = calloc(1, sizeof(Tree));
    t->prodrule = 0;
    t->nChildren = 0;
    t->children = NULL;
    t->filename = filename;
    t->leaf = tok;
    return t;
}


Tree* allocate_tree(int p, int argc, ...) {
    Tree* tempnode;
    va_list ap;
    va_start(ap, argc);

    Tree* ret = calloc(1, sizeof(Tree));
    ret->children = calloc(argc, sizeof(Tree*));
    // Fill out ret->children
    int i;
    for (i = 0; i<argc; i++) {
        tempnode = va_arg(ap, Tree*); 
        ret->children[i] = tempnode;
    }
    va_end(ap); 
    
    ret->nChildren = argc;
    ret->prodrule = p;
    ret->filename = filename;
    ret->leaf = NULL;
    return ret;
}


// Add the name of a class to the list of defined classes
void add_class(Tree* t) {
    if (NUMCLASSES != 0) {
        CLASSES = realloc(CLASSES, NUMCLASSES*sizeof(char*));
    } else {
        CLASSES = calloc(NUMCLASSES, sizeof(char*));
    }
    CLASSES[NUMCLASSES] = t->leaf->text;
    NUMCLASSES++;
}


// Check if the given string is the name of an already defined class
int is_class(char* test, char** classes, int numclasses) {
    int i;
    for (i = 0; i<numclasses; i++) {
        if (strcmp(test, classes[i]) == 0) {
            return 1;
        }
    }
    return 0;
}


void print_tree(Tree* t, int depth) {
    char* prodrule;
    int i;
    if (t == NULL) {
        printf("%*sEmpty Rule\n", depth*2, " ");
        return;
    } else if (t->prodrule == 0) {
        printf("%*sToken: %s\n", depth*2, " ", t->leaf->text);
        return;
    }

    prodrule = pretty_print_name(t->prodrule);
    printf("%*s%s: %d\n", depth*2, "", prodrule, t->nChildren);

    for(i=0; i<t->nChildren; i++) {
        print_tree(t->children[i], depth+1);
    }
}

char* get_basefile(char* infile){
    char* fname = strdup(infile);
    int i = 0, j=0, k = 0;
    while (fname[i] != '\0') { // find last slash
        if (fname[i] == '/') {
            j = i;
        } else if (fname[i] == '.') {
            k = i;
        }
        i++;
    }
    if (!k) {
        k = i;
    }

    for (i = 0; i < (k-j-1); i++) {
        fname[i] = infile[j+i+1];
    }

    FNAME_END = i;
    fname[i] = '\0';
    return fname;
}

void print_code(char* infile, Tree* root) {
    if (!root) return;
    char* fname = strcatnew(infile, ".ic", "\0");
    int i = FNAME_END;
    /* fname[i] = '.'; */
    /* fname[i+1] = 'i'; */
    /* fname[i+2] = 'c'; */
    /* fname[i+3] = '\0'; */
    FILE* f = fopen(fname, "w");
    if (f == NULL) {
        fprintf(stderr, "Could not open file %s for writing\n", fname);
    }
    Instruction* ins = root->code;

    time_t rawtime;
    struct tm * timeinfo;
    time ( &rawtime );
    timeinfo = localtime ( &rawtime );
    fprintf (f, "Current local time and date: %s", asctime(timeinfo));

    if (NSTRINGS) fprintf(f, ".string\n");
    for (i = 0; i < NSTRINGS; i++) {
        fprintf(f, "\t%s\n", STRINGS_LIST[i]);
    }
    if (NDOUBLES) fprintf(f, ".double\n");
    for (i = 0; i < NDOUBLES; i++) {
        fprintf(f, "\t%lg\n", DOUBLES_LIST[i]);
    }

    fprintf(f, ".code\n");
    while (ins != NULL) {
        if (ins->opcode == D_PROC) {
            fprintf(f, "proc %s,%i,%i\n", ins->sval, ins->src1->offset, ins->dest->offset);
        } else if (ins->opcode == D_CLASS) {
            fprintf(f, "class %s,%i,%i\n", ins->sval, ins->src1->offset, ins->dest->offset);
        } else if (ins->opcode == O_CALL) {
            fprintf(f, "\t%s\t%s,%i,%s:%i\n", pretty_print_opcode(ins->opcode), ins->sval, ins->src1->offset, pretty_print_region(ins->dest->region), ins->dest->offset);
        } else if (ins->opcode == O_NEW) {
            fprintf(f, "\tNEW\t%s,%i\n", ins->sval, ins->src1->offset);
        } else if (ins->opcode == O_MEMBER) {
            fprintf(f, "\tMEMBER\t%s:%i,%s:%i,%s\n", pretty_print_region(ins->dest->region), ins->dest->offset, pretty_print_region(ins->dest->region), ins->src1->offset, ins->sval);
        } else if (ins->opcode == D_STRING) {
            fprintf(f, "%s", ins->sval);
        } else {
            fprintf(f, "\t%s", pretty_print_opcode(ins->opcode));
            if (ins->dest) {
                fprintf(f, "\t%s:%i", pretty_print_region(ins->dest->region), ins->dest->offset);
            }
            if (ins->src1) {
                fprintf(f, ",%s:%i", pretty_print_region(ins->src1->region), ins->src1->offset);
            }
            if (ins->src2) {
                fprintf(f, ",%s:%i", pretty_print_region(ins->src2->region), ins->src2->offset);
            }
            fprintf(f, "\n");
        }
        ins = ins->next;
    }
    fclose(f);
}

void compile(char* basefile) { 
   char* outstring = malloc(1000); // TODO: Get right size
   sprintf(outstring, "gcc %s.s -o %s", basefile, basefile);
   printf("%s\n", outstring);
   system(outstring);
   free(outstring);
}

// Generates assembly code. Stores it in $basefile.s
void gen_as(char* basefile, struct instr* code) {
    // Here I would generate the assembly from my intermediate code.
    // I would do something like this for each instruction

    /*
    open %.s file
    Instruction* c = code;
    while (c != NULL) {
        switch (c) {
            case OPCODE:
                // Determine which registers are free
                // clear registers that will need to be used for the given instruction
                // Load src1 and src2 from the stack into a register (if they aren't already)
                // Execute the given instruction
                break;
        }
    }
    */

    // Generate basic assembly that can be linked.
    char* outfile = strcatnew(basefile, ".s", "\0");
    printf("Saving to %s\n", outfile);
    FILE* f = fopen(outfile, "w+");
    if (!f) { fprintf(stderr, "Error: Could not open file %s\n", outfile);}
    fprintf(f, " .data\n hello_str:\n .string \"Hey there Dr. Jeffery!\\nUnfortunately, I didn\\'t have time to work on this homework assignment. It only generates this program, runs gcc to compile the assembly, and print this message. I\\'m sorry I didn\\'t have the time to complete this assignment, but I learned a lot from this class and I\\'m glad I took it!\\n\"\n .set hello_str_length, . - hello_str - 1\n .text\n .globl  main\n .type   main, @function\n main:\n movl    $4, %%eax\n movl    $1, %%ebx\n movl    $hello_str, %%ecx\n movl    $hello_str_length, %%edx\n int     $0x80\n movl    $1, %%eax\n movl    $0, %%ebx\n int     $0x80\n .size   main, . - main\n ");
    fclose(f);
}

// Runs the assemble. Assumes $basefile.s exists. Creates $basefile.o
void gen_o(char* basefile) { 
   char* outstring = malloc(1000); // TODO: Get right size
   /* sprintf(outstring, "env > temp_output.txt; gcc -v -c %s.s -o %s.o >> temp_output.txt 2>&1", basefile, basefile); */
   sprintf(outstring, "gcc -c %s.s -o %s.o", basefile, basefile);
   printf("%s\n", outstring);
   system(outstring);
   free(outstring);
}

// Returns a new string that is the concatenation of c1 and c2
// WARNING: Mallocs a new string, obviously.
char* strcatnew(char* c1, char* c2, char* delimit) {
    char* cnew = malloc(sizeof(char)*(strlen(c1)+strlen(c2)));
    int i = 0, j = 0;
    while (c1[i] != '\0') {
        cnew[i] = c1[i];
        i++;
    }
    while (delimit[j] != '\0') {
        cnew[i] = delimit[j];
        i++; j++;
    } j = 0;
    while (c2[j] != '\0') {
        cnew[i] = c2[j];
        i++; j++;
    }
    cnew[i] = '\0';
    return cnew;
}

#endif
