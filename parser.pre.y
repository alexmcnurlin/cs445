/* g0 Grammar */
/* Derived from the Godiva Grammar, which was in turn */
/* based on the Java grammar in Gosling/Joy/Steele, Chapter 19 */
/* Modified by Alex McNurlin for University of Idaho CS445. */
/* mcnu5088@vandals.uidaho.edu */
%expect 70

%{
#include <stdio.h>
#include <string.h>

#include "errors.h"
#include "graph.h"
#include "helper.h"
#include "prodrules.h"
#include "structs.h"

extern int yylex();
extern char* yytext;
extern int yylineno;
int yyerror(const char *s);
struct tree* root;

/* Global constants to help work around reduce/reduce errors */
int TL_DEC = 1;
int CLASS_DEC = 1;
int IS_PROTOTYPE = 0;

%}

%locations

%union {
    struct tree *node;
}


###TOKENS

###PRODRULES

%right ASN 
%right PLASN 
%right MIASN 
%right SWAP

%start Goal

%%

Goal:
    TopLevelList                        {$$ = $1; root = $$;}
    | { $$ = NULL; }
    ;

TopLevelList:
    TopLevel
    | TopLevel TopLevelList             { $$ = allocate_tree(TOPLEVELLIST, 2, $1, $2); }
    ;

TopLevel:
    FunctionDef                         { $$ = $1; TL_DEC = 0;}
    | ClassDef                          { $$ = $1; TL_DEC = 0; }
    | VariableDeclaration               { $$ = $1; if (!TL_DEC) { yyerror(NO_VAR_DECLARATION1); } }
    ;

Statement:
    IfStatement
    | WhileStatement
    | ForStatement
    | Assignment
    | FunctionLikeCall
    | RETURN Expression                 { $$ = allocate_tree(RETURN_STATEMENT, 1, $2); }
    | RETURN                            { $$ = allocate_tree(RETURN_STATEMENT, 1, NULL); }
    | BREAK
    | Statement SEMI                    { yyerror(SEMI_ERROR); }
    | Type IDENT ASN Expression         { yyerror(ASSIGNMENT_ERROR); }
    ;

FunctionLikeCall:
    FunctionCall
    | MethodCall
    | ConstructorCall
    | DataMemberCall
    ;

DataMemberCall:
    IDENT DOT IDENT                     { $$ = allocate_tree(DATAMEMBERACCESS, 2, $1, $3); }
    ;

DeclarationList:
    VariableDeclaration
    | VariableDeclaration DeclarationList { $$ = allocate_tree(DECLARATIONS, 2, $1, $2); }
    ;

VariableDeclaration:
    Type IDENT                          { $$ = allocate_tree(VARIABLEDECLARATION, 2, $1, $2); }
    | Type IDENT VariableDeclarationList   { $$ = allocate_tree(VARIABLEDECLARATIONLIST, 3, $1, $2, $3); }
    | VariableDeclaration ASN Expression { yyerror(INITIALIZATION_ERROR); }
    ;

VariableDeclarationList:
    COMMA IDENT                         { $$ = $2; }
    | COMMA IDENT VariableDeclarationList { $$ = allocate_tree(VARIABLEDECLARATIONLISTCONTENTS, 2, $2, $3); }
    ;

Assignment:
    MultiAssignment ASN Expression           { $$ = allocate_tree(ASSIGNMENT, 2, $1, $3); }
    | MultiSwap SWAP Expression                 { $$ = allocate_tree(SWAPPERATOR, 2, $1, $3); }
    | Assignable PLASN StringCatOpt          { $$ = allocate_tree(INCREMENT, 2, $1, $3); }
    | Assignable MIASN StringCatOpt          { $$ = allocate_tree(DECREMENT, 2, $1, $3); }
    ;

Assignable:
    IDENT
    | IDENT DOT IDENT                        { $$ = allocate_tree(DATAMEMBERACCESS, 2, $1, $3); }
    | IDENT LB Expression RB                 { $$ = allocate_tree(ARRAYACCESS, 2, $1, $3); }
    | IDENT LB RB                            { $$ = allocate_tree(ARRAYACCESS, 2, $1, NULL); }
    ;

MultiAssignment:
    MultiAssignment ASN Expression           { $$ = allocate_tree(MULTI_ASSIGNMENT, 2, $1, $3); }
    | Assignable
    ;

MultiSwap:
    MultiSwap SWAP Expression                     { $$ = allocate_tree(MULTI_SWAP, 2, $1, $3); }
    | Assignable             
    ;

/* This is a less than ideal way of supporting string concatenation */
/* It allows an identifier followed by a string, or *only* 2 adjacent strings to be concatenated */
StringCatOpt:
    Expression
    | IDENT StringLiteral
    /* | StringLiteral IDENT // This causes 'i = "hello" \n world = "error"' to parse as 'i = ("hello" world) (= "error")', which is unacceptable */
    /* | StringLiteral StringLiteral */
    | StringLiteral StringLiteral
    ;

ConstructorCall:
    CLASSNAME LP ArgList RP             { $$ = allocate_tree(CONSTRUCTORCALL, 2, $1, $3); }
    | CLASSNAME LP RP                   { $$ = allocate_tree(CONSTRUCTORCALL, 2, $1, NULL); }
    ;

FunctionCall:
    IDENT LP ArgList RP                 { $$ = allocate_tree(FUNCTIONCALL, 2, $1, $3); }
    | IDENT LP RP                       { $$ = allocate_tree(FUNCTIONCALL, 2, $1, NULL); }
    | WRITE LP RP                       { $$ = allocate_tree(WRITE_CALL, 1, NULL); }
    | WRITE LP ArgStringCatList RP      { $$ = allocate_tree(WRITE_CALL, 1, $3); }
    | READ LP RP                        { $$ = allocate_tree(READ_CALL, 1, NULL); }
    /* | READ LP ArgStringCatList RP       { $$ = allocate_tree(READ_CALL, 1, $3); } */
    ;

ArgStringCatList:
    StringCatList
    | StringCatList COMMA ArgStringCatList { $$ = allocate_tree(WRITE_ARGLIST, 2, $1, $3); }
    ;

StringCatList:
    Expression StringCatList            { $$ = allocate_tree(STRINGCATLIST, 2, $1, $2); }
    | Expression
    ;

IfStatement:
    IF LP Expression RP LC StatementList RC ElseIf { $$ = allocate_tree(IFELSESTATEMENT, 3, $3, $6, $8); }
    | IF LP Expression RP LC StatementList RC { $$ = allocate_tree(IFSTATEMENT, 2, $3, $6); }
    ;

ElseIf:
    ELSE IF LP Expression RP LC StatementList RC ElseIf { $$ = allocate_tree(IFELSELIST, 3, $4, $7, $9); }
    | ELSE IF LP Expression RP LC StatementList RC      { $$ = allocate_tree(TRAILINGELSEIF, 2, $4, $7); }
    | ELSE LC StatementList RC                          { $$ = allocate_tree(TRAILINGELSE, 1, $3); }
    ;

WhileStatement:
    WHILE LP Expression RP LC StatementList RC { $$ = allocate_tree(WHILESTATEMENT, 2, $3, $6); }
    ;

ForStatement:
    FOR LP ExpressionOrAssnOpt SEMI ExpressionOpt SEMI StatementOpt RP LC StatementList RC { $$ = allocate_tree(FORSTATEMENT, 4, $3, $5, $7, $10); }
    | FOR LP ExpressionOrAssnOpt SEMI ExpressionOpt SEMI StatementOpt RP LC RC { $$ = allocate_tree(FORSTATEMENT, 4, $3, $5, $7, NULL); }
    ;

ExpressionOpt:
    Expression
    | { $$ = NULL; }
    ;

StatementOpt:
    Statement
    | { $$ = NULL; }
    ;

ExpressionOrAssnOpt:
    Expression
    | Assignment
    | { $$ = NULL; }
    ;

StatementList:
    StatementListEnd
    | { $$ = NULL; }
    ;

StatementListEnd:
    Statement
    | Statement StatementListEnd        { $$ = allocate_tree(STATEMENTLIST, 2, $1, $2); }
    ;

FunctionDef:
    Type IDENT LP ArgDefList RP LC {ARGCHECK;} FunctionBody RC    { $$ = allocate_tree(FUNCTIONDEF, 4, $1, $2, $4, $8); }
    | Type IDENT LP RP LC FunctionBody RC                         { $$ = allocate_tree(FUNCTIONDEF, 4, $1, $2, NULL, $6); }
    | VOID IDENT LP ArgDefList RP LC {ARGCHECK;} FunctionBody RC  { $$ = allocate_tree(FUNCTIONDEF, 4, $1, $2, $4, $8); }
    | VOID IDENT LP RP LC FunctionBody RC                         { $$ = allocate_tree(FUNCTIONDEF, 4, $1, $2, NULL, $6); }
    ;

FunctionBody:
    DeclarationList StatementList       { $$ = allocate_tree(FUNCTIONBODY_W_DECL, 2, $1, $2); }
    | StatementList                     { $$ = allocate_tree(FUNCTIONBODY_NO_DECL, 1, $1); }
    ;

ClassDef:
    CLASS IDENT LC {CLASS_DEC=1;add_class($2);} ClassBody RC         { $$ = allocate_tree(CLASSDEF, 3, $1, $2, $5); }
    ;

ClassBody:
    ClassBodyDef
    | ClassBodyDef ClassBody 	        { $$ = allocate_tree(CLASSBODYLIST, 2, $1, $2); }
    ;

ClassBodyDef:
    MethodDef                           { $$ = $1; CLASS_DEC = 0; }
    | VariableDeclaration               { $$ = $1; if (!CLASS_DEC) { yyerror(NO_VAR_DECLARATION2); }; }
    ;

MethodDef:
    CLASSNAME LP ArgDefList RP LC {ARGCHECK;} FunctionBody RC { $$ = allocate_tree(CONSTRUCTOR, 3, $1, $3, $7); }
    | CLASSNAME LP RP LC FunctionBody RC          { $$ = allocate_tree(CONSTRUCTOR, 3, $1, NULL, $5); }
    | Type IDENT LP ArgDefList RP LC {ARGCHECK;} FunctionBody RC    { $$ = allocate_tree(METHODDEF, 4, $1, $2, $4, $8); }
    | Type IDENT LP RP LC FunctionBody RC                         { $$ = allocate_tree(METHODDEF, 4, $1, $2, NULL, $6); }
    | VOID IDENT LP ArgDefList RP LC {ARGCHECK;} FunctionBody RC  { $$ = allocate_tree(METHODDEF, 4, $1, $2, $4, $8); }
    | VOID IDENT LP RP LC FunctionBody RC                         { $$ = allocate_tree(METHODDEF, 4, $1, $2, NULL, $6); }
    ;

Type:
    INT
    | BOOLEAN
    | DOUBLE
    | STRING
    | CLASSNAME
    | LIST                              { $$ = allocate_tree(LISTTYPE, 1, NULL); }
    | LIST LT Type GT                   { $$ = allocate_tree(LISTTYPE, 1, $3); }
    | TABLE LT Type COMMA Type GT       { $$ = allocate_tree(TABLETYPE, 2, $3, $5); }
    | TABLE LT Type GT                  { $$ = allocate_tree(TABLETYPE, 2, NULL, $3); }
    | TABLE                             { $$ = allocate_tree(TABLETYPE, 2, NULL, NULL); }
    ;

ArgDefList:
    { RESET_ARG; } ArgDefListEnd        { $$ = $2; }
    ;

ArgDefListEnd:
    ArgDef COMMA ArgDefList             { $$ = allocate_tree(ARGDEFLIST, 2, $1, $3); }
    | ArgDef
    ;

ArgDef:
    Type IDENT                          { $$ = allocate_tree(ARG_DEF_LIST, 2, $1, $2); }
    | Type                              { IS_PROTOTYPE = 1; $$ = $1; }
    ;

ArgList:
    Expression
    | Expression COMMA ArgList          { $$ = allocate_tree(ARGLIST, 2, $1, $3); }
    ;

Expression:
    Exp5
    ;

Exp5:
    Exp5 AND Exp4             { $$ = allocate_tree(AND_EXP, 2, $1, $3); }
    | Exp5 OR Exp4            { $$ = allocate_tree(OR_EXP, 2, $1, $3); }
    | Exp4
    ;

Exp4:
    Exp4 EQ Exp3              { $$ = allocate_tree(EQUAL, 2, $1, $3); }
    | Exp4 NE Exp3            { $$ = allocate_tree(NOT_EQUAL, 2, $1, $3); }
    | Exp3
    ;

Exp3:
    Exp3 LT Exp2              { $$ = allocate_tree(LESS_THAN, 2, $1, $3); }
    | Exp3 LTE Exp2           { $$ = allocate_tree(LESS_THAN_EQUAL, 2, $1, $3); }
    | Exp3 GT Exp2            { $$ = allocate_tree(GREATER_THAN, 2, $1, $3); }
    | Exp3 GTE Exp2           { $$ = allocate_tree(GREATER_THAN_EQUAL, 2, $1, $3); }
    | Exp2
    ;

Exp2:
    Exp2 PLUS Exp1            { $$ = allocate_tree(ADD, 2, $1, $3); }
    | Exp2 MINUS Exp1         { $$ = allocate_tree(SUBTRACT, 2, $1, $3); }
    | Exp1
    ;

Exp1:
    Exp1 MUL PostfixExp         { $$ = allocate_tree(MULTIPLICATION, 2, $1, $3); }
    | Exp1 DIV PostfixExp       { $$ = allocate_tree(DIVISION, 2, $1, $3); }
    | Exp1 MOD PostfixExp       { $$ = allocate_tree(MODULUS, 2, $1, $3); }
    | Exp1 DICE PostfixExp      { $$ = allocate_tree(BIN_DIE_ROLL, 2, $1, $3); }
    | PostfixExp
    ;

PostfixExp:
    PostfixExp DOT IDENT            { $$ = allocate_tree(DATAMEMBERACCESS, 2, $1, $3); }
    | PostfixExp LB Expression RB   { $$ = allocate_tree(ARRAYACCESS, 2, $1, $3); }
    | PostfixExp LB Expression COLON Expression RB   { $$ = allocate_tree(SPLICE, 3, $1, $3, $5); }
    | PostfixExp DOT IDENT LP RP    { $$ = allocate_tree(METHODCALL, 3, $1, $3, NULL); }
    | PostfixExp DOT IDENT LP ArgList RP      { $$ = allocate_tree(METHODCALL, 3, $1, $3, $5); }
    | ExpEnder
    ;

ExpEnder:
    Literal
    | IDENT
    | FunctionCall
    /* | MethodCall */
    | ConstructorCall
    /* | DataMemberCall */
    | UnaryExpression
    | LP Expression RP            { $$ = allocate_tree(GROUPED_EXP, 1, $2); }
    ;

MethodCall:
    IDENT DOT IDENT LP RP            { $$ = allocate_tree(METHODCALL, 3, $1, $3, NULL); }
    | IDENT DOT IDENT LP ArgList RP  { $$ = allocate_tree(METHODCALL, 3, $1, $3, $5); }
    ;

UnaryExpression:
    MINUS Expression                    { $$ = allocate_tree(UNARY_MINUS, 1, $2); }
    | BANG Expression                   { $$ = allocate_tree(UNARY_NOT, 1, $2); }
    | DICE Expression                   { $$ = allocate_tree(UNARY_DI, 1, $2); }
    | SIZE Expression                   { $$ = allocate_tree(UNARY_SIZE, 1, $2); }
    ;

Literal:
    INTLITERAL
    | REALLITERAL
    | ArrayLiteral
    | TableLiteral
    | StringLiteral
    | BoolLiteral
    ;

BoolLiteral:
    TRUE
    | FALSE
    ;

StringLiteral:
    STRINGLITERAL
    | STRINGLITERAL StringLiteral    { $$ = allocate_tree(IMPLICIT_STRCAT, 2, $1, $2); }
    ;

TableLiteral:
    LC TableEntry RC                    { $$ = allocate_tree(TABLELITERAL, 1, $2); }
    | LC RC                             { $$ = allocate_tree(EMPTYTABLE, 1, NULL); }
    ;

TableEntry:
    Expression COLON Expression      { $$ = allocate_tree(TABLESINGLEENTRY, 2, $1, $3); }
    | Expression COLON Expression COMMA TableEntry { $$ = allocate_tree(TABLEMULTIENTRY, 3, $1, $3, $5); }
    ;

ArrayLiteral:
    LB ArrayContents RB                 { $$ = allocate_tree(ARRAYLITERAL, 1, $2); }
    | LB RB                             { $$ = allocate_tree(ARRAYLITERAL, 1, NULL); }
    ;

ArrayContents:
    Expression COMMA ArrayContents      { $$ = allocate_tree(ARRAYCONTENTS, 2, $1, $3); }
    | Expression
    ;

%%
