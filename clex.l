D        [0-9]
L        [a-zA-Z]
L_nd     [a-ce-zA-Z]
H        [a-fA-F0-9]
E        [Ee][+-]?{D}+
FS       (f|F|l|L)
IS       (u|U|l|L)
W        [ \t\f]*
LIT      \"(\\.|[^\\"])*\"

%{

#include "helper.h"
#include "parser.tab.h"
#include "structs.h"

extern int yychar;
extern YYSTYPE yylval;
extern char* classNames;
extern char** CLASSES;
extern int NUMCLASSES;

int line_num = 1;
int errors;
int included_iostream = 0;

%}
%option yylineno
%option nounput
%option noinput


%%

\n                           { line_num++; }
[ \t\f]+                     { }

"//".*                       { /* yylval.node = allocate_leaf(COMMENT); return COMMENT; // These can be uncommented if we want to tokenize comments*/ }
"/*"([^*]|"*"+[^/*])*"*"+"/" { /* yylval.node = allocate_leaf(COMMENT); return COMMENT; */ }
"break"                      { yylval.node = allocate_leaf(BREAK); return BREAK; }
"bool"                       { yylval.node = allocate_leaf(BOOLEAN); return BOOLEAN; }
"class"                      { yylval.node = allocate_leaf(CLASS); return CLASS; }
"double"                     { yylval.node = allocate_leaf(DOUBLE); return DOUBLE; }
"else"                       { yylval.node = allocate_leaf(ELSE); return ELSE; }
"false"                      { yylval.node = allocate_leaf(FALSE); return FALSE; }
"for"                        { yylval.node = allocate_leaf(FOR); return FOR; }
"if"                         { yylval.node = allocate_leaf(IF); return IF; }
"int"                        { yylval.node = allocate_leaf(INT); return INT; }
"list"                       { yylval.node = allocate_leaf(LIST); return LIST; }
"null"                       { yylval.node = allocate_leaf(NULLLITERAL); return NULLLITERAL; }
"return"                     { yylval.node = allocate_leaf(RETURN); return RETURN; }
"string"                     { yylval.node = allocate_leaf(STRING); return STRING; }
"table"                      { yylval.node = allocate_leaf(TABLE); return TABLE; }
"true"                       { yylval.node = allocate_leaf(TRUE); return TRUE; }
"void"                       { yylval.node = allocate_leaf(VOID); return VOID; }
"while"                      { yylval.node = allocate_leaf(WHILE); return WHILE; }
"write"                      { yylval.node = allocate_leaf(WRITE); return WRITE; }
"read"                      { yylval.node = allocate_leaf(READ); return READ; }

\"(\\.|[^\\"])*\"            { yylval.node = allocate_leaf(STRINGLITERAL); return STRINGLITERAL; /* taken from http://www.lysator.liu.se/c/ANSI-C-grammar-l.html */ }
\'(\\.|[^\\'])*\'            { yylval.node = allocate_leaf(STRINGLITERAL); return STRINGLITERAL; /* taken from http://www.lysator.liu.se/c/ANSI-C-grammar-l.html */ }

"("                          { yylval.node = allocate_leaf(LP); return LP; }
")"                          { yylval.node = allocate_leaf(RP); return RP; }
"{"                          { yylval.node = allocate_leaf(LC); return LC; }
"}"                          { yylval.node = allocate_leaf(RC); return RC; }
"["                          { yylval.node = allocate_leaf(LB); return LB; }
"]"                          { yylval.node = allocate_leaf(RB); return RB; }
","                          { yylval.node = allocate_leaf(COMMA); return COMMA; }
"."                          { yylval.node = allocate_leaf(DOT); return DOT; }
";"                          { yylval.node = allocate_leaf(SEMI); return SEMI; }
":"                          { yylval.node = allocate_leaf(COLON); return COLON; }

-                            { yylval.node = allocate_leaf(MINUS); return MINUS; }
!                            { yylval.node = allocate_leaf(BANG); return BANG; }
d/[0-9]*                     { yylval.node = allocate_leaf(DICE); return DICE; /* Note, this will lex d20 as 'd' and '20' */}
#                            { yylval.node = allocate_leaf(SIZE); return SIZE; }

"*"                          { yylval.node = allocate_leaf(MUL); return MUL; }
"/"                          { yylval.node = allocate_leaf(DIV); return DIV; }
"%"                          { yylval.node = allocate_leaf(MOD); return MOD; }

"+"                          { yylval.node = allocate_leaf(PLUS); return PLUS; }

"<="                         { yylval.node = allocate_leaf(LTE); return LTE;}
">="                         { yylval.node = allocate_leaf(GTE); return GTE;}
"<"                          { yylval.node = allocate_leaf(LT); return LT; }
">"                          { yylval.node = allocate_leaf(GT); return GT; }
"=="                         { yylval.node = allocate_leaf(EQ); return EQ; }
"!="                         { yylval.node = allocate_leaf(NE); return NE; }
"&&"                         { yylval.node = allocate_leaf(AND); return AND; }
"||"                         { yylval.node = allocate_leaf(OR); return OR; }

"="                          { yylval.node = allocate_leaf(ASN); return ASN; }
"+="                         { yylval.node = allocate_leaf(PLASN); return PLASN; }
"-="                         { yylval.node = allocate_leaf(MIASN); return MIASN; }
":=:"                        { yylval.node = allocate_leaf(SWAP); return SWAP; }

{D}+                         { yylval.node = allocate_leaf(INTLITERAL); return INTLITERAL; }
{L}({L}|{D}){0,11}           { if (is_class(yytext, CLASSES, NUMCLASSES)) {yylval.node = allocate_leaf(CLASSNAME); return CLASSNAME;} 
                               else {yylval.node = allocate_leaf(IDENT); return IDENT; /* This won't match identifiers longer than 12 characters. Anything longer will be lexed as multiple lexemes */}}
(0|[1-9][0-9]*)(\.[0-9]+)?([eE][-+]?[0-9]+)? { yylval.node = allocate_leaf(REALLITERAL); return REALLITERAL; /* Any valid JSON Number */ }
<<EOF>>                      { yylval.node = NULL; return -1; /* Return -1 on EOF, instead of 0 */ }
.                            { yylval.node = NULL; lexerr(yytext, yylineno); }

%%

/* Return 1 if done, 0 if yyin points at more input */
int yywrap()
{
    return 1;
}

