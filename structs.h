#ifndef TREE_H
#define TREE_H

#include <stdlib.h>
#include <stdio.h>

#include "parser.tab.h"


typedef struct token {
    int category;   /* the integer code returned by yylex */
    char *text;     /* the actual string (lexeme) matched */
    int lineno;     /* the line number on which the token occurs */
    char *filename; /* the source file in which the token occurs */
    int ival;       /* for integer constants, store binary value here */
    double dval;    /* for real constants, store binary value here */
    char *sval;     /* for string constants, malloc space, de-escape, store */
} Token;


typedef struct param {
    char *name;
    struct typeinfo *type;
    struct param *next;
    int size;
} ParamList;


typedef struct typeinfo {
    int basetype;
    union {
        struct arrayinfo {
            struct typeinfo *elemtype;
        } a;
        struct tableinfo {
            struct typeinfo *keytype;
            struct typeinfo *valtype;
        } t;
        struct classinfo {
            char *name;
            struct sym_table *st;
        } c;
        struct methodinfo {
            char *name;
            struct param *parameters;
            struct sym_table *st;
            struct typeinfo *returntype;
        } m;
        struct constructorinfo {
            struct sym_table *st;
            struct typeinfo *returntype;
            struct param *parameters;
        } con;
    } u;
} TypeInfo;


typedef struct sym_table {
    int nBuckets;           /* # of buckets */
    int nEntries;           /* # of symbols in the table */
    int size;                /* Size (in bytes) of all the elements in the table */
    struct sym_table *parent;       /* enclosing scope, superclass etc. */
    TypeInfo* scope;            /* what type do we belong to? */
    struct sym_entry **tbl;
    /* more per-scope/per-symbol-table attributes go here */
} SymbolTable;


/* Entry in symbol table. */
typedef struct sym_entry {
    SymbolTable* table;         /* what symbol table do we belong to*/
    TypeInfo* type;
    char *s;                /* string */
    int size;
    struct addr* place;
    struct sym_entry *next;
} SymbolTableEntry;


typedef struct tree {
    SymbolTable* table;
    TypeInfo* type;
    char* filename;
    int lineno;
    int nChildren;
    int prodrule;
    struct addr* place;
    struct instr* code;
    struct token* leaf;
    struct tree** children;
} Tree;


typedef struct addr {
    int region;
    int offset;
} Address;


typedef struct instr {
    int opcode;
    struct addr* dest;
    struct addr* src1;
    struct addr* src2;
    struct instr *next;
    char* sval;
} Instruction;

#endif
