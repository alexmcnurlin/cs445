#!/usr/bin/env python3

def main():
    prodrules = 0
    with open("symbols.txt", "r") as f:
        tokens = ""
        prods = ""
        prod_defs = ""
        i = 1
        contents = f.read()
        for symbol in contents.split("\n")[:-1]:
            if symbol == "#PRODRULES":
                prodrules = 1
            if symbol and symbol[0] not in  ("#", "\n", " "):
                if not prodrules:
                    tokens += "%token < node > {}\n".format(symbol)
                else:
                    prods += "%type < node > {}\n".format(symbol)
                    prod_defs += "#DEFINE c{} {}\n".format(symbol, i)
                    i += 1
        contents = ""
        with open("parser.pre.y", "r") as y:
            contents = y.read()
            contents = contents.replace("###TOKENS", tokens)
            contents = contents.replace("###PRODRULES", prods)
        with open("parser.y", "w") as y:
            y.write(contents)

if __name__ == "__main__":
    main()
