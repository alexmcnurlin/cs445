STD=$()
SRCS=clex.l parser.y main.c helper.c makefile graph.c symt.c prodrules.c type.c codegen.c tac.c
SRCS_H=structs.h main.h helper.h prodrules.h errors.h graph.h type.h symt.h codegen.h tac.h
EXTRA=input/ output/ README.md
ARGS=$( -g )
BIN=g0
HW=hw6

all: $(BIN)

## COMPILING
###############################################################################
$(BIN): lex.yy.o main.o helper.o parser.tab.o parser.tab.h structs.h graph.o symt.o prodrules.o type.o codegen.o tac.o 
	gcc -Wall lex.yy.o parser.tab.o main.o helper.o graph.o symt.o prodrules.o type.o codegen.o tac.o -o $(BIN)

parser.tab.c parser.tab.h: parser.y prodrules.h
	bison -vkd parser.y

lex.yy.c: clex.l parser.tab.h
	flex clex.l

%.o: %.c
	gcc -Wall $(GOPT) -g -c $< -o $@

main.o: main.c errors.h structs.h helper.h parser.tab.h main.h graph.h symt.h type.h codegen.h
	gcc -Wall $(GOPT) -g -c $< -o $@

helper.o: helper.c structs.h helper.h parser.tab.h main.h prodrules.h type.h structs.h tac.h
	gcc -Wall $(GOPT) -g -c $< -o $@

test.o: test.c structs.h parser.tab.h helper.h
	gcc -Wall $(GOPT) -g -c $< -o $@

graph.o: graph.c structs.h helper.h parser.tab.h
	gcc -Wall $(GOPT) -g -c $< -o $@

symt.o: symt.c errors.h helper.h symt.h parser.tab.h prodrules.h structs.h parser.tab.h type.h tac.h
	gcc -Wall $(GOPT) -g -c $< -o $@

type.o: type.c type.h symt.h parser.tab.h prodrules.h helper.h
	gcc -Wall $(GOPT) -g -c $< -o $@

codegen.o: codegen.c codegen.h structs.h tac.h symt.h helper.h
	gcc -Wall $(GOPT) -g -c $< -o $@

tac.o: tac.c tac.h structs.h prodrules.h codegen.h
	gcc -Wall $(GOPT) -g -c $< -o $@

###REMOVE
parser.y: parser.pre.y symbols.txt generate_symbols.py
	python3 generate_symbols.py

prodrules.h prodrules.c: parser.pre.y parse_prodrules.py
	python3 parse_prodrules.py parser.pre.y prodrules

###ENDREMOVE

## TESTING
###############################################################################
test: unit_tests func_tests
	@

unit_test_exe: test.o helper.o lex.yy.o parser.tab.o main.o main.h parser.tab.h symt.h symt.o type.o prodrules.o
	@gcc test.o helper.o lex.yy.o parser.tab.o symt.o type.o prodrules.o -o unit_test_exe

unit_tests: unit_test_exe
	@./unit_test_exe

COPY=
func_tests: $(BIN)
	-for myfile in $(shell ls input); do \
		echo "#####################################"; \
		echo "Testing $$myfile"; \
		./$(BIN) -s input/$$myfile > /tmp/$$myfile 2>&1; \
		diff /tmp/$$myfile output/sym_$$myfile && echo "PASS"; \
		./$(BIN) -t input/$$myfile > /tmp/$$myfile 2>&1; \
		diff /tmp/$$myfile output/tree_$$myfile && echo "PASS"; \
			###REMOVE \
			# cp /tmp/$$myfile output/$$myfile && echo "PASS"; \
			# code --diff /tmp/$$myfile output/$$myfile && echo "PASS"; \
			###ENDREMOVE \
	done

copy: input/$(file) $(BIN)
	-./g0 $(opt) input/$(file) > output/sym_$(file) 2>&1

## EXTRA
###############################################################################
run: $(BIN)
	./$(BIN) $(opt) $(file)

run-graph: $(BIN)
	-./$(BIN) -g $(file)

bison:  parser.tab.h parser.tab.c

debug: $(BIN)
	gdb -q $(BIN)

clean:
	git clean -fX # Remove files ignored in .gitignore
	#rm -f temp
	#rm -f unit_test_exe
	#rm -f y.tab.c y.tab.h parser.output
	#rm -f *.o *.gch parser.tab.*
	#rm -f *.png *.dot
	#rm -f $(BIN) lex.yy.c
	####REMOVE
	#rm -f parser.y symbols.h
	####ENDREMOVE

deepclean: clean
	rm -rf build/
	rm -f *.tar *.zip

graph_full.png: run-graph parse_tree_full.dot
	sed -i parse_tree_full.dot -e s/\"\"/\"\\\"/g
	sed -i parse_tree_full.dot -e "s/[()]//g"
	dot -Tpng parse_tree_full.dot -o graph_full.png

graph_simple.png: run-graph parse_tree_simple.dot
	sed -i parse_tree_simple.dot -e "s/\"\"/\"\\\"/g"
	sed -i parse_tree_simple.dot -e "s/[()]//g"
	dot -Tpng parse_tree_simple.dot -o graph_simple.png

G=simple
graph: graph_$(G).png
	eog graph_$(G).png

## SUBMISSION
###############################################################################
tar: mcnu5088_$(HW).tar

zip: mcnu5088_$(HW).zip

###REMOVE
build: $(BIN) $(SRCS) $(SRCS_H) $(EXTRA) parse_makefile.py
	mkdir -p build
	cp -r $(SRCS) $(SRCS_H) $(EXTRA) build
	python3 parse_makefile.py build/makefile
###ENDREMOVE

mcnu5088_$(HW).tar: build clean $(SRCS)
	cd build; tar -cvf ../mcnu5088_$(HW).tar *; cd ..

mcnu5088_$(HW).zip: build $(SRCS)
	cd build; zip -r ../mcnu5088_$(HW).zip .; cd ..

###REMOVE
commit: build tar zip clean
	git add .
	git commit -m "$(HW): Saving work"
	git push

HOST=cs-course42.cs.uidaho.edu
upload: commit zip
	scp mcnu5088_$(HW).zip mcnu5088$(HOST):~/cs445/mcnu5088_$(HW).zip
	ssh mcnu5088@$(HOST)

HOST=wormulon.cs.uidaho.edu
upload_wormulon:
	scp mcnu5088_$(HW).zip mcnu5088\@$(HOST):~/cs445/mcnu5088_$(HW).zip
	ssh mcnu5088\@$(HOST)

###ENDREMOVE
