#ifndef MAIN_C
#define MAIN_C

#include <stdlib.h>
#include <string.h>

#include "codegen.h"
#include "errors.h"
#include "graph.h"
#include "helper.h"
#include "main.h"
#include "parser.tab.h"
#include "structs.h"
#include "symt.h"
#include "type.h"

char* filename = NULL;
int PRINT_GRAPH = 0;
int PRINT_SYMT = 0;
int PRINT_TREE = 0;
int PRINT_ASM = 0;
int PRINT_COMP = 0;

extern FILE* yyin;
extern Tree* root;
extern char* yytext;
extern int CLASS_DEC;
extern int IS_PROTOTYPE;
extern int TL_DEC;
extern int errors;
extern int yylineno;
extern int yyrestart();
extern SymbolTable* current;
extern int ERROR_TYPES;

int main(int argc, char** argv) {

    int start = 1;
    if (argc > 1 && strcmp(argv[1], "-g") == 0) {
        // If we're given -g as the first argument, generate a graph for the parse tree
        PRINT_GRAPH = 1;
        start = 2;
    } else if (argc > 1 && strcmp(argv[1], "-y") == 0) {
        // If we're given -s as the first argument, print the symbol tables
        PRINT_SYMT = 1;
        start = 2;
    } else if (argc > 1 && strcmp(argv[1], "-t") == 0) {
        // If we're given -s as the first argument, print the symbol tables
        PRINT_TREE = 1;
        start = 2;
    } else if (argc > 1 && strcmp(argv[1], "-s") == 0) {
        // If we're given -s as the first argument, print the symbol tables
        PRINT_ASM = 1;
        start = 2;
    } else if (argc > 1 && strcmp(argv[1], "-c") == 0) {
        // If we're given -s as the first argument, print the symbol tables
        PRINT_COMP = 1;
        start = 2;
    }

    if (argc == start) {
    // Check for filename passed as arg
    // If no filename is passed, read from stdin
        filename = "/dev/stdin";
        argv[start] = strdup(filename);
        argc = start+1;
    }

    // Iterate over each file 
    int i;
    for (i = start; i < argc; i++) {
        // Reset buffer on new file
        if (i != start) {
            yyrestart(yyin);
            fclose(yyin);
            TL_DEC = 1;
            CLASS_DEC = 1;
            IS_PROTOTYPE = 0;
        }
        filename = verify_filename(argv[i]);
        yyin = fopen(filename, "r");
        printf("Reading from file %s\n", filename);
        if (yyin == NULL) { // If file can't open, print error and go to next file
            fprintf(stderr, "Could not open file %s\n", argv[i]);
            continue;
        }
        // Start parsing!
        yylineno = 1;
        yyparse();

        populate_symboltables(root);
        semantic_analysis(root);

        char* basefile = get_basefile(filename);
        if (!ERROR_TYPES) {
            codegen(root);
            print_code(basefile, root);

            if (PRINT_ASM) {
                gen_as(basefile, root->code);
            } else if (PRINT_COMP) {
                gen_as(basefile, root->code);
                gen_o(basefile);
            } else {
                gen_as(basefile, root->code);
                gen_o(basefile);
                compile(basefile);
            }
        }


        // Print things
        if (PRINT_GRAPH) {
            print_graph(root, 0);
        } else if (PRINT_SYMT) {
            print_st(current, 0);
        } else if (PRINT_TREE) {
            print_tree(root, 0);
        }

        // Error codes
        if (ERROR_TYPES) {
            if ((ERROR_TYPES & LEXICAL_ERROR) == LEXICAL_ERROR) {
                exit(1);
            } else if ((ERROR_TYPES & SYNTAX_ERROR) == SYNTAX_ERROR) {
                exit(2);
            } else if ((ERROR_TYPES & SEMANTIC_ERROR) == SEMANTIC_ERROR) {
                exit(3);
            }
        } else {
            printf("Parser exited succesfully\n");
        }
        errors = 0;
    }

    return 0;
}


#endif
