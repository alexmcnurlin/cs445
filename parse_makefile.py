#! /usr/bin/env python3
import re
import sys

strip = re.compile(r"^[^\n]*###REMOVE.*?###ENDREMOVE", re.DOTALL | re.MULTILINE)
with open("makefile", 'r') as f:
    makefile = f.read()
    makefile = strip.sub("", makefile)
    with open(sys.argv[1], 'w') as g:
        g.write(makefile)
