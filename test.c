#include <assert.h>
#include <string.h>
#include "structs.h"
#include "type.h"
#include "parser.tab.h"
#include "helper.h"
#include "symt.h"

extern char* yytext;
extern int line_num;
extern char* filename;
extern YYSTYPE yylval;

Tree* create_leaf(char* s);


// This file will be used to test C functions defined for this assignment
int main() {
    // Suppress errors
    stderr = fopen("/dev/null", "w");

    ///////////////////////////////////////////////////////////////////////////
    // Parse string
        printf("Parse string 1\n");
        char *input = "\"Hello\\nworld\\\\how\\\"are\\tyou\"";
        char *retval = strdup(input);
        parse_string(retval, input);
        assert(strcmp(retval, "Hello\nworld\\how\"are\tyou") == 0);
        printf("Pass\n");

        printf("Parse string 2\n");
        input = "\"Basic string\"";
        retval = strdup(input);
        parse_string(retval, input);
        assert(strcmp(retval, "Basic string") == 0);
        printf("Pass\n");


    ///////////////////////////////////////////////////////////////////////////
    // Allocate leaf
        printf("Allocate Leaf 1\n");
        yytext = "Test";
        line_num = 12;
        Tree* leaf = allocate_leaf(12);
        assert(leaf->leaf != NULL);     // Verify token was allocated
        assert(leaf->nChildren == 0);
        assert(leaf->children == NULL);
        assert(strcmp(leaf->leaf->text, "Test") == 0);
        assert(leaf->leaf->category == 12);
        printf("Pass\n");

    ///////////////////////////////////////////////////////////////////////////
    // Allocate Tree
        printf("Allocate Tree 1\n");
        yytext = "Test";
        line_num = 12;
        Tree* leaf1 = allocate_leaf(12);
        Tree* leaf2 = allocate_leaf(13);
        Tree* leaf3 = allocate_leaf(14);
        Tree* root = allocate_tree(1, 3, leaf3, leaf1, leaf2);
        assert(root->nChildren == 3);
        assert(root->children[0] == leaf3);
        assert(root->children[1] == leaf1);
        assert(root->children[2] == leaf2);
        assert(root->prodrule == 1);
        printf("Pass\n");

    ///////////////////////////////////////////////////////////////////////////
    // Symbol Table
        printf("Create symbol table\n");
        SymbolTable* st = new_st(10);
        assert(st->nBuckets == 10);
        assert(st->nEntries == 0);
        printf("Pass\n");

        printf("Insert into Symbol table\n");
        int h = insert_sym(st, create_leaf("Test"), NULL);
        SymbolTableEntry* se = st->tbl[h];
        assert(st->nEntries == 1);
        assert(se->table == st);
        assert(se->next == NULL);
        assert(strcmp(se->s, "Test") == 0);
        printf("Pass\n");

        printf("Insert into Symbol table 2\n");
        h = insert_sym(st, create_leaf("Test2"), NULL);
        assert(st->nEntries == 2);
        printf("Pass\n");

        printf("Insert into Symbol table 3\n");
        h = insert_sym(st, create_leaf("Test8"), NULL);
        SymbolTableEntry* se2 = st->tbl[h];
        assert(st->nEntries == 3);
        assert(se2->table == st);
        assert(se2->next == se);
        assert(strcmp(se2->s, "Test8") == 0);
        printf("Pass\n");

        printf("Insert into Symbol table 4\n");
        h = insert_sym(st, create_leaf("Test"), NULL);
        assert(st->nEntries == 3);
        printf("Pass\n");

        printf("Delete symbol table\n");
        int old1 = st->nBuckets;
        delete_st(st);
        // When st gets freed, all it's members become garbage
        assert(st->nBuckets != old1);
        printf("Pass\n");
}

Tree* create_leaf(char* s) {
        yytext = s;
        return allocate_leaf(1);
}
