#include <stdio.h>
#include <string.h>
#include "helper.h"
#include "parser.tab.h"
#include "prodrules.h"
#include "symt.h"
#include "type.h"

struct typeinfo integer_type = { INT };
struct typeinfo string_type = { STRING };
struct typeinfo double_type = { DOUBLE };
struct typeinfo bool_type = { BOOLEAN };
struct typeinfo void_type = { NULLLITERAL };
TypeInfo* integer_typeinfo = &integer_type;
TypeInfo* string_typeinfo = &string_type;
TypeInfo* double_typeinfo = &double_type;
TypeInfo* bool_typeinfo = &bool_type;
TypeInfo* void_typeinfo = &void_type;
int TYPEERROR;

extern int tree_break;

TypeInfo* alctype(int base) {
    switch (base) {
        case INT:
            return integer_typeinfo;
        case DOUBLE:
            return double_typeinfo;
        case STRING:
            return string_typeinfo;
        case BOOLEAN:
            return bool_typeinfo;
        case NULLLITERAL:
            return void_typeinfo;
        case CLASS:
            break;
        case CLASSNAME: // I don't think this is necessary
            break;
    }
    TypeInfo* rv;
    rv = (TypeInfo*) calloc(1, sizeof(struct typeinfo));
    rv->basetype = base;
    return rv;
}

TypeInfo* alcclasstype(Tree* t, SymbolTable* st) {
    TypeInfo* rv = alctype(CLASS);
    rv->u.c.name = strdup(t->leaf->text);
    rv->u.c.st = st;
    return rv;
}

TypeInfo* alcfunctype(Tree* f, SymbolTable* st) {
    TypeInfo* rv = alctype(FUNCTIONDEF);
    rv->u.m.st = st;
    rv->u.m.returntype = get_type(f->children[0]);
    rv->u.m.name = f->children[1]->leaf->text;
    rv->u.m.parameters = get_parameters(f->children[2]);
    return rv;
}

/* in order for this to make any sense, you have to pass in the subtrees
 * for the return type (r) and the parameter list (p), but the calls to
 * to this function in the example are just passing NULL at present!
 */
TypeInfo* alcmethodtype(Tree* m, SymbolTable* st) {
    TypeInfo* rv = alctype(METHODDEF);
    rv->u.m.st = st;
    rv->u.m.returntype = get_type(m->children[0]);
    rv->u.m.name = m->children[1]->leaf->text;
    rv->u.m.parameters = get_parameters(m->children[2]);
    return rv;
}

TypeInfo* alcconstructortype(Tree* c, SymbolTable* st) {
    TypeInfo* rv = alctype(CONSTRUCTOR);
    rv->u.con.st = st;
    rv->u.con.returntype = get_type(c->children[0]);
    rv->u.con.parameters = get_parameters(c->children[1]);
    return rv;
}

// Check that the two typeinfo objects represent compatible types
// If types equal, return the TypeInfo* and set TYPEERROR to 0
// If types differ, set TYPEERROR to 1 and return t1
// If either is NULL, return NULL and set TYPEERROR to 0
// In all of the above cases, if the return value is not in types
//      set TYPEERROR to 1
TypeInfo* compare_type(TypeInfo* t1, TypeInfo* t2, int types) {
    TYPEERROR = 0;
    TypeInfo* retval;
    if (t1 == NULL && t2 != NULL) {
        retval = t2;
    } else if (t1 == NULL || t2 == NULL) {
        return NULL;
    } else if (t1->basetype == t2->basetype) {
        switch(t1->basetype) {
            case LISTTYPE:
                compare_type(t1->u.a.elemtype, t2->u.a.elemtype, M_ANY);
                break;
            case TABLETYPE:
                compare_type(t1->u.t.keytype, t2->u.t.keytype, M_ANY);
                compare_type(t1->u.t.valtype, t2->u.t.valtype, M_ANY);
                break;
            case CLASS:
                // This pointer comparison relies on the fact that all 
                // instances of a class use the same typeinfo
                if (t1 != t2) {
                    TYPEERROR = 1;
                }
                break;
        }
        retval = t1;
    } else if (t1->basetype == INT && t2->basetype == DOUBLE) { // Promote ints to doubles
        return t2;
    } else if (t1->basetype == DOUBLE && t2->basetype == INT) { // Promote ints to doubles
        return t1;
    } else {
        TYPEERROR = 1;
        retval = t1;
    }
    if (!valid_type(types, retval)) {
        TYPEERROR = 1;
    }
    return retval;
}   

int valid_type(int types, TypeInfo* t1) {
    switch (t1->basetype) {
        case INT:
            return types & M_INT;
        case DOUBLE:
            return types & M_DOUBLE;
        case STRING:
            return types & M_STRING;
        case BOOLEAN:
            return types & M_BOOL;
        case VOID:
            return types & M_VOID;
        case CLASS:
            return types & M_CLASS;
        case LISTTYPE:
            return types & M_LIST;
        case TABLETYPE:
            return types & M_TABLE;
        default:
            return 0;
    }
}

TypeInfo* typecheck_children(Tree* n, int valid_types) {
    int i, rule = n->prodrule;
    TypeInfo* type = NULL, *temp = NULL;
    for (i=0; i<n->nChildren; i++) {
        if (n->children[i]) {
            temp = compare_type(type, n->children[i]->type, valid_types);
            if (TYPEERROR) {
                if (valid_types && type != NULL) {
                    typeerror_mult(type, n->children[i]->type, n, valid_types);
                } else {
                    typeerror1(valid_types, n->children[i]->type, n);
                }
                return temp;
            } else if (temp->basetype == DOUBLE && n->children[i]->type->basetype == INT) {
                if (rule == SWAPPERATOR || rule == MULTI_ASSIGNMENT || rule == MULTI_SWAP ) {
                    typeerror1(M_DOUBLE, integer_typeinfo, n);
                } else {
                    promote_to_double(n, i);
                }
            } else if (type && type->basetype == INT && n->children[i]->type->basetype == DOUBLE) {
                if ((rule == ASSIGNMENT && i == 1) || rule == SWAPPERATOR || rule == MULTI_ASSIGNMENT || rule == MULTI_SWAP ) {
                    typeerror1(M_INT, double_typeinfo, n);
                } else {
                    promote_to_double(n, i-1);
                }
            }
            type = temp;
        }
    }
    return type;
}

TypeInfo* typecheck_function(SymbolTable* st, Tree* n, Tree* funcname, Tree* in_params, int functype) {
    SymbolTableEntry* ste;
    TypeInfo* type;
    ParamList* real_params;
    switch (functype) {
        case FUNCTIONDEF:
        case METHODDEF:
            ste = get_entry(st, funcname);
            if (!ste) {semanticerror("Not a valid funcion", funcname); tree_break=1; return NULL;}
            type = ste->type->u.m.returntype;
            real_params = ste->type->u.m.parameters;
            break;
        case CONSTRUCTOR:
            ste = get_entry(st, funcname);
            ste = get_entry(ste->type->u.c.st, funcname);
            type = ste->type->u.con.returntype;
            real_params = ste->type->u.con.parameters;
            break;
    }
    if (ste->type->basetype != METHODDEF && ste->type->basetype != FUNCTIONDEF && ste->type->basetype != CONSTRUCTOR) {
        semanticerror("Member is not a function:", funcname);
        tree_break = 1;
        return NULL;
    }
    if (real_params == NULL && in_params == NULL) {
        return type;
    } else if (real_params != NULL && in_params == NULL) {
        typeerror3(1, funcname->leaf->text, real_params->type, NULL, n);
    } else {
        int i = 1;
        // Check first to second to last args
        while (in_params->prodrule == ARGLIST) {
            if (real_params == NULL) {
                typeerror4(i, funcname->leaf->text, in_params);
                return type;
            }
            compare_type(real_params->type, in_params->children[0]->type, M_ANY);
            if (TYPEERROR) {
                typeerror3(i, funcname->leaf->text, real_params->type, in_params->children[0]->type, in_params);
            }
            in_params = in_params->children[1];
            real_params = real_params->next;
            i++;
        }

        // Check last arg
        if (real_params == NULL) {
            typeerror4(i, funcname->leaf->text, in_params);
            return type;
        }
        compare_type(real_params->type, in_params->type, M_ANY);
        if (TYPEERROR) {
            typeerror3(i, funcname->leaf->text, real_params->type, in_params->type, in_params);
        }

        // Check potential extra args
        if (real_params->next != NULL) {
            typeerror3(i, funcname->leaf->text, real_params->type, NULL, n);
        }
    }
    return type;
}

int is_assignable(Tree* n) {
    switch(n->prodrule) {
        case 0:
            switch (n->leaf->category) {
                case IDENT:
                    return 1;
                default:
                    return 0;
            }

        case MULTI_ASSIGNMENT:
        case MULTI_SWAP:
        case ARRAYACCESS:
        case DATAMEMBERACCESS:
            return 1;
            break;
        default:
            return 0;
            break;
    }
}

void promote_to_double(Tree* n, int i) {
    /* n->type = double_typeinfo; */
    Tree* conv = calloc(1, sizeof(Tree));
    conv->prodrule = INT_TO_DOUBLE;
    conv->nChildren = 1;
    conv->children = calloc(1, sizeof(Tree*));
    conv->children[0] = n->children[i];
    conv->type = double_typeinfo;
    n->children[i] = conv;
}

TypeInfo* perform_typechecking(Tree* n) {
    TypeInfo* type = NULL;
    SymbolTableEntry* ste = NULL;
    SymbolTable* st = NULL;
    switch (n->prodrule) {
        case 0:
            switch (n->leaf->category) {
                case IDENT:
                    ste = get_entry(current, n);
                    type = ste->type;
                    break;
                case INTLITERAL:
                    type = alctype(INT);
                    break;
                case REALLITERAL:
                    type = alctype(DOUBLE);
                    break;
                case STRINGLITERAL:
                    type = alctype(STRING);
                    break;
                case TRUE:
                case FALSE:
                    type = alctype(BOOLEAN);
                    break;
            }
            break;

        case ADD:
            type = typecheck_children(n, M_INT | M_DOUBLE | M_STRING | M_LIST);
            break;

        case AND_EXP:
        case OR_EXP:
            type = typecheck_children(n, M_BOOL);
            break;

        case ARRAYACCESS:
            switch (n->children[0]->type->basetype) {
                case LISTTYPE:
                    if (!n->children[1]) { // li[] = 0 // Set default element to 0
                        type = n->children[0]->type->u.a.elemtype;
                        break;
                    }
                    // Assert RHS is int for array access
                    compare_type(NULL, n->children[1]->type, M_INT);
                    if (TYPEERROR) {
                        typeerror1(M_INT, n->children[1]->type, n);
                    }
                    type = n->children[0]->type->u.a.elemtype;
                    break;
                case TABLETYPE:
                    type = n->children[0]->type->u.t.keytype;
                    // Assert RHS is int for array access
                    if (n->children[1]) {
                        compare_type(type, n->children[1]->type, M_ANY);
                        if (TYPEERROR) {
                            typeerror1_alt(type, n->children[1]->type, n);
                        }
                    }
                    type = n->children[0]->type->u.t.valtype;
                    break;
                case STRING:
                    compare_type(NULL, n->children[1]->type, M_INT);
                    if (TYPEERROR) {
                        typeerror1(M_INT, n->children[1]->type, n);
                    }
                    type = alctype(STRING);
                    break;
                default:
                    typeerror1(M_LIST | M_TABLE | M_STRING, n->children[0]->type, n);
            }
            break;

        case SPLICE:
            compare_type(NULL, n->children[1]->type, M_INT);
            if (TYPEERROR) {
                typeerror1(M_INT, n->children[1]->type, n);
            }
            compare_type(NULL, n->children[2]->type, M_INT);
            if (TYPEERROR) {
                typeerror1(M_INT, n->children[2]->type, n);
            }
            type = alctype(STRING);
            break;

        case ARRAYCONTENTS:
            type = typecheck_children(n, M_ANY);
            break;

        case ARRAYLITERAL:
            type = alctype(LISTTYPE);
            type->u.a.elemtype = n->children[0] ? n->children[0]->type : NULL;
            break;

        case ASSIGNMENT:
            if (!is_assignable(n->children[0])) {
                semanticerror_prod("Cannot perform assignment to operand", n);
            }
            type = typecheck_children(n, M_ANY);
            break;

        case SWAPPERATOR:
            if (!is_assignable(n->children[0]) || !is_assignable(n->children[1])) {
                semanticerror_prod("Cannot perform assignment to operand", n);
            }
            type = typecheck_children(n, M_ANY);
            break;

        case MULTI_ASSIGNMENT:
            if (!is_assignable(n->children[0]) || !is_assignable(n->children[1])) {
                semanticerror_prod("Cannot perform assignment to operand", n);
            }
            type = typecheck_children(n, M_ANY);
            break;

        case MULTI_SWAP:
            if (!is_assignable(n->children[0]) || !is_assignable(n->children[1])) {
                semanticerror_prod("Cannot perform assignment to operand", n);
            }
            type = typecheck_children(n, M_ANY);
            break;

        case BIN_DIE_ROLL:
            type = typecheck_children(n, M_INT);
            break;

        case CONSTRUCTORCALL:
            ste = get_entry(current, n->children[0]);
            type = typecheck_function(current, n, n->children[0], n->children[1], CONSTRUCTOR);
            break;

        case DATAMEMBERACCESS:
            if (n->children[0]->type && n->children[0]->type->u.c.st) {
                st = n->children[0]->type->u.c.st;
                ste = get_entry(st, n->children[1]);
                if (!ste || ste->type->basetype == FUNCTIONDEF || ste->type->basetype == METHODDEF) {
                    semanticerror("Invalid member:", n->children[1]);
                    tree_break = 1;
                    return NULL;
                }
                type = ste->type;
            } else {
                    semanticerror("Left hand side of dot operator is not a class:", n->children[0]);
                    tree_break = 1;
                    return NULL;
            }
            break;

        case DECREMENT:
            // This allows table t1 -= 1
            if (n->children[0]->type->basetype==TABLETYPE && n->children[1]->type->basetype!=TABLETYPE) {
                compare_type(n->children[0]->type->u.t.keytype, n->children[1]->type, M_ANY);
                if (TYPEERROR) {
                    typeerror1_alt(n->children[0]->type->u.a.elemtype, n->children[1]->type, n);
                }
            } else {
                typecheck_children(n, M_INT | M_DOUBLE);
            }
            break;

        case INCREMENT:
            // This allows list l1 += 1
            if (n->children[0]->type->basetype==LISTTYPE && n->children[1]->type->basetype!=LISTTYPE) {
                compare_type(n->children[0]->type->u.a.elemtype, n->children[1]->type, M_ANY);
                if (TYPEERROR) {
                    typeerror1_alt(n->children[0]->type->u.a.elemtype, n->children[1]->type, n);
                }
            } else {
                // normal incrememnt e.g. i += 1
                typecheck_children(n, M_INT | M_DOUBLE | M_STRING | M_LIST);
            }
            break;

        case DIVISION:
        case MULTIPLICATION:
            type = typecheck_children(n, M_INT | M_DOUBLE);
            break;

        case EMPTYTABLE:
            type = alctype(TABLETYPE);
            type->u.t.keytype = NULL;   // Any table can receive them
            type->u.t.valtype = NULL;
            break;

        case EQUAL:
        case NOT_EQUAL:
            typecheck_children(n, M_INT | M_DOUBLE | M_STRING | M_BOOL);
            type = bool_typeinfo;
            break;

        case FORSTATEMENT:
            if (n->children[1]) {
                compare_type(NULL, n->children[1]->type, M_BOOL);
                if (TYPEERROR) {
                    typeerror1(M_BOOL, n->children[1]->type, n);
                }
            }
            break;

        case FUNCTIONCALL:
            type = typecheck_function(current, n, n->children[0], n->children[1], FUNCTIONDEF);
            break;

        case GREATER_THAN:
        case GREATER_THAN_EQUAL:
        case LESS_THAN:
        case LESS_THAN_EQUAL:
            typecheck_children(n, M_INT | M_DOUBLE);
            type = bool_typeinfo;
            break;

        case GROUPED_EXP:
            type = n->children[0]->type;
            break;

        case METHODCALL:
            type = n->children[0]->type;
            type = typecheck_function(type->u.c.st, n, n->children[1], n->children[2], METHODDEF);
            break;

        case IFELSELIST:
        case IFELSESTATEMENT:
        case IFSTATEMENT:
        case TRAILINGELSEIF:
            compare_type(NULL, n->children[0]->type, M_BOOL);
            if (TYPEERROR) {
                typeerror1(M_BOOL, n->children[0]->type, n);
            }
            break;

        case IMPLICIT_STRCAT:
            type = typecheck_children(n, M_STRING);
            break;

        case MODULUS:
            type = typecheck_children(n, M_INT);
            break;

        case READ_CALL:
            type = alctype(STRING);
            break;

        case RETURN_STATEMENT:
            if (n->children[0] == NULL) {
                type = compare_type(current->scope->u.m.returntype, void_typeinfo, M_ANY);
                break;
                if (TYPEERROR) {
                    typeerror1_alt(current->scope->u.m.returntype, void_typeinfo, n);
                }
            } else {
                type = compare_type(current->scope->u.m.returntype, n->children[0]->type, M_ANY);
                if (TYPEERROR) {
                    typeerror1_alt(current->scope->u.m.returntype, n->children[0]->type, n);
                }
            }
            break;

        case SUBTRACT:
            type = typecheck_children(n, M_INT | M_DOUBLE);
            break;

        case STRINGCATLIST:
            type = compare_type(NULL, n->children[0]->type, M_STRING | M_INT | M_DOUBLE);
            if (TYPEERROR) {
                typeerror1(M_STRING | M_INT | M_DOUBLE, n->children[0]->type, n);
            }
            type = compare_type(NULL, n->children[1]->type, M_STRING | M_INT | M_DOUBLE);
            if (TYPEERROR) {
                typeerror1(M_STRING | M_INT | M_DOUBLE, n->children[1]->type, n);
            }
            break;

        case UNARY_DI:
            type = typecheck_children(n, M_INT);
            break;

        case UNARY_NOT:
            type = typecheck_children(n, M_BOOL);
            break;

        case UNARY_SIZE:
            typecheck_children(n, M_LIST | M_TABLE | M_STRING);
            type = alctype(INT);
            break;

        case UNARY_MINUS:
            type = typecheck_children(n, M_INT | M_DOUBLE);
            break;

        case TABLEMULTIENTRY:
            type = alctype(TABLETYPE);
            type->u.t.keytype = n->children[0]->type;
            type->u.t.valtype = n->children[1]->type;
            type = compare_type(type, n->children[2]->type, M_TABLE);
            if (TYPEERROR) {
                typeerror1_alt(type, n->children[2]->type, n);
            }
            break;

        case TABLESINGLEENTRY:
            type = alctype(TABLETYPE);
            type->u.t.keytype = n->children[0]->type;
            type->u.t.valtype = n->children[1]->type;
            break;

        case TABLELITERAL:
            type = n->children[0]->type;
            break;

        case WHILESTATEMENT:
            compare_type(NULL, n->children[0]->type, M_BOOL);
            if (TYPEERROR) {
                typeerror1(M_BOOL, n->children[0]->type, n);
            }
            break;

        case WRITE_ARGLIST:
            type = typecheck_children(n, M_STRING);
            break;

        case WRITE_CALL:
            typecheck_children(n, M_STRING);
            type = void_typeinfo;
            break;
    }
    return type;
}
