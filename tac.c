// Three Address Code - skeleton for CS 445
#include <stdio.h>
#include <stdlib.h>

#include "codegen.h"
#include "prodrules.h"
#include "structs.h"
#include "tac.h"

extern SymbolTable* current;
extern int NSTRINGS;
extern int NDOUBLES;

int NLABLES = 0;

Instruction* gen(int op, Address* a1, Address* a2, Address* a3) {
    Instruction *rv = calloc(1, sizeof(Instruction));
    if (rv == NULL) {
        fprintf(stderr, "out of memory\n");
        exit(4);
    }
    rv->opcode = op;
    rv->dest = a1;
    rv->src1 = a2;
    rv->src2 = a3;
    rv->next = NULL;
    return rv;
}

Instruction* gen_proc(int op, char* name, int nargs, int nlocals) {
    Instruction *rv = calloc(1, sizeof(Instruction));
    rv->opcode = op;
    rv->dest = newconst(nlocals);
    rv->sval = name;
    rv->src1 = newconst(nargs);
    return rv;
}

Instruction* gen_call(int op, char* name, int nParams, Address* retloc) {
    Instruction *rv = calloc(1, sizeof(Instruction));
    rv->opcode = op;
    rv->dest = retloc;
    rv->sval = name;
    rv->src1 = newconst(nParams);
    return rv;
}

Instruction* get_parms_code(Tree* n) {
    Tree* t = n;
    Instruction* code = NULL;
    if (n == NULL) {return NULL;}
    while (t->prodrule == ARGLIST) {
        code = append(gen(O_PARM, t->children[0]->place, NULL, NULL), code);
        t = t->children[1];
    }
    code = append(gen(O_PARM, t->place, NULL, NULL), code);
    return code;
}

Instruction* get_list_code(Tree* n, Address* dest) {
    Tree* t = n;
    Instruction* code = NULL;
    Address* t1;
    if (n == NULL) {return NULL;}
    int i = 0;
    t1 = newtemp();
    while (t->prodrule == ARRAYCONTENTS) {
        code = append(code, gen(O_ADIND+7, t1, dest, newconst(i)));
        code = append(code, gen(O_SCONT,   t1, t->children[0]->place, NULL));
        t = t->children[1];
        i++;
    }
    code = append(code, gen(O_ADIND+7, t1, dest, newconst(i)));
    code = append(code, gen(O_SCONT,   t1, t->place, NULL));
    return code;
}

Instruction* get_table_code(Tree* n, Address* dest) {
    Tree* t = n;
    Instruction* code = NULL;
    Address* t1;
    if (n == NULL) {return NULL;}
    t1 = newtemp();
    while (t->prodrule == TABLEMULTIENTRY) {
        code = append(code, gen(O_ADIND+8, t1, dest, t->children[0]->place));
        code = append(code, gen(O_SCONT,   t1, t->children[1]->place, NULL));
        t = t->children[2];
    }
    code = append(code, gen(O_ADIND+7, t1, dest, t->children[0]->place));
    code = append(code, gen(O_SCONT,   t1, t->children[1]->place, NULL));
    return code;
}

int get_list_size(Tree* n) {
    Tree* t = n;
    if (n == NULL) {return 0;}
    int i = 0;
    while (t->prodrule == ARRAYCONTENTS) {
        i++;
        t = t->children[1];
    }
    return ++i;
}

int get_n_args(Tree* n) {
    Tree* t = n;
    int nargs = 0;
    if (n == NULL) {return 0;}
    while (t->prodrule == ARGLIST) {
        nargs += 1;
        t = t->children[1];
    }
    nargs += 1;
    return nargs;
}

Instruction* copylist(Instruction* l) {
    if (l == NULL) return NULL;
    Instruction *lcopy = gen(l->opcode, l->dest, l->src1, l->src2);
    lcopy->sval = l->sval;
    lcopy->next = copylist(l->next);
    return lcopy;
}

Instruction* append(Instruction* l1, Instruction* l2) {
    if (l1 == NULL) return l2;
    Instruction *ltmp = l1;
    while (ltmp->next != NULL) {ltmp = ltmp->next;}
    ltmp->next = l2;
    return l1;
}

Instruction* concat(Instruction* l1, Instruction* l2) {
    return append(copylist(l1), l2);
}

Address* newloc(SymbolTable* st, int region, int size) {
    Address *p = calloc(1, sizeof(p));
    p->region = region;
    p->offset = st->size;
    st->size += size;
    return p;
}

Address* newconst(int val) {
    Address *p = calloc(1, sizeof(p));
    p->region = R_CONST;
    p->offset = val;
    return p;
}

Address* newtemp() {
    Address* n = calloc(1, sizeof(Address*));
    n->region = R_LOCAL;
    n->offset = current->size;
    current->size += 8;
    return n;
}

Address* newstring() {
    Address* n = calloc(1, sizeof(Address*));
    n->region = R_STRING;
    n->offset = 8*(NSTRINGS-1);
    return n;
}

Address* newlabel() {
    Address* n = calloc(1, sizeof(Address*));
    n->region = R_LABEL;
    n->offset = NLABLES;
    NLABLES += 1;
    return n;
}
Address* newdouble() {
    Address* n = calloc(1, sizeof(Address*));
    n->region = R_DOUBLE;
    n->offset = 8*(NDOUBLES-1);
    return n;
}

char* pretty_print_region(int region) {
    switch (region) {
        case R_GLOBAL:
            return "global";
        case R_LOCAL:
            return "local";
        case R_CLASS:
            return "class";
        case R_LABEL:
            return "label";
        case R_CONST:
            return "const";
        case R_UNDEF:
            return "undefined";
        case R_STRING:
            return "string";
        case R_DOUBLE:
            return "double";
        default:
            return NULL;
    }
}

char* pretty_print_opcode(int op) {
    char *code, *retval, *fstring = "%c%s";
    int type = 0;
    char types[] = {'\0', 'I', 'D', 'S', 'B', '\0', '\0', 'T', 'L'};
    // types[1] == int
    // types[2] == double
    // types[3] == string
    // types[4] == bool
    // types[5] == not used
    // types[6] == special case for table
    // types[7] == table
    // types[8] == list
    // types[9] == special case for list

    // Print the opcodes
    switch (op) {
        // Special cases (i.e. opcode depends on type)
        case O_INDEX + 8: // List index
            return "LINDEX";
        case O_INDEX + 7: // Table index
            return "TINDEX";
        case O_ADIND + 8: // Fetch address of list index e.g. &(L[0])
            return "LADIND";
        case O_ADIND + 7: // Fetch address of table index e.g. &(T["zero"])
            return "TADIND";
        case O_ADD + 8: // t = l1 + l2
            return "LCONCAT";
        case O_LAPPEND: // l1 += l2
            return "LAPPEND";
        case O_ADD + 9: // l1 += i
            return "LPUT";
        case O_SUB + 6: // t1 -= i
            return "TDELETE";
        case O_TDEFAULT: // t1[] = 1
            return "TDEFAULT";
        case O_ADD + 3: // "string" + "concatenation"
            return "SCONCAT";
        case O_LSTRIP:  // Strip the first n characters of a string
            return "LSTRIP";
        case O_RSTRIP: // Strip the characters after index n of a string
            return "RSTRIP";

        // General cases
        // Prepend opcode with type, if applicable
        // e.g. DADD => Double Add
        default:
           switch ((op/10)*10) { // replace last digit with 0. +1 for int division
                case O_ADD:
                    code = "ADD"; break;
                case O_SUB:
                    code = "SUB"; break;
                case O_MUL:
                    code = "MUL"; break;
                case O_DIV:
                    code = "DIV"; break;
                case O_NEG:
                    code = "NEG"; break;
                case O_ASN:
                    code = "ASN"; break;
                case O_ADDR:
                    code = "ADDR"; break;
                case O_LCONT:
                    code = "LCONT"; break;
                case O_SCONT:
                    code = "SCONT"; break;
                case O_GOTO:
                    code = "GOTO"; break;
                case O_BLT:
                    code = "BLT"; break;
                case O_BLE:
                    code = "BLE"; break;
                case O_BGT:
                    code = "BGT"; break;
                case O_BGE:
                    code = "BGE"; break;
                case O_BEQ:
                    code = "BEQ"; break;
                case O_BNE:
                    code = "BNE"; break;
                case O_BIF:
                    code = "BIF"; break;
                case O_BNIF:
                    code = "BNIF"; break;
                case O_PARM:
                    code = "PARM"; break;
                case O_CALL:
                    code = "CALL"; break;
                case O_RET:
                    code = "RET"; break;
                case O_ITOD: // Int to double
                    return "ITOD"; break;
                case D_GLOB:
                    code = "GLOB"; break;
                case D_PROC:
                    code = "PROC"; break;
                case D_LOCAL:
                    code = "LOCAL"; break;
                case D_LABEL:
                    code = "LABEL"; break;
                case D_FIELD:
                    code = "FIELD"; break;
                case D_END:
                    code = "END"; break;
                case O_LT:
                    code = "LT"; break;
                case O_GT:
                    code = "GT"; break;
                case O_OR:
                    code = "OR"; break;
                case O_AND:
                    code = "AND"; break;
                case O_EQ:
                    code = "EQ"; break;
                case O_NEQ:
                    code = "NEQ"; break;
                case O_SIZE:
                    code = "SIZE"; break;
                case O_NEW:
                    return "NEW"; break;
                case O_MEMBER:
                    return "MEMBER"; break;
                case O_RAND:
                    return "RAND"; break;
                case O_MOD:
                    return "MOD"; break;
                case O_NOT:
                    return "NOT"; break;
                case O_LIST:
                    return "LIST"; break;
                case O_TABLE:
                    return "TABLE"; break;
                default:
                    return "UNDEFINED";
            }
            break;
    }
    
    type = types[op%10];
    if (type != '\0') {
        retval = calloc(10, sizeof(char));
        sprintf(retval, fstring, type, code);
    } else {
        retval = code;
    }
    return retval;
}

Instruction* emptylist() {
    return NULL;
}

int op_type(TypeInfo* type) {
    switch (type->basetype) {
        case INT: return 1;
        case DOUBLE: return 2;
        case STRING: return 3;
        case BOOLEAN: return 4;
        case TABLETYPE: return 7;
        case LISTTYPE: return 8;
    }
    fprintf(stderr, "Invalid type when getting opcode");
    return 0;
}

int size_vars(SymbolTable* st) {
    int h, n=0;
    SymbolTableEntry* se;

    for (h = 0; h < st->nBuckets; h++) {
        for (se = st->tbl[h]; se != NULL; se = se->next) {
            switch (se->type->basetype) {
                case METHODDEF:
                case FUNCTIONDEF:
                case CLASSDEF:
                case CONSTRUCTOR:
                    break;
                default:
                    n += se->size;
            }
        }
    }
    return n;
}

int size_methods(SymbolTable* st){ 
    int h, n=0;
    SymbolTableEntry* se;

    for (h = 0; h < st->nBuckets; h++) {
        for (se = st->tbl[h]; se != NULL; se = se->next) {
            switch (se->type->basetype) {
                case FUNCTIONDEF:
                case METHODDEF:
                case CONSTRUCTOR:
                    n += se->size;
                    break;
            }
        }
    }
    return n;
}
